package net.customware.confluence.flotchart.util;

import junit.framework.TestCase;
import net.customware.confluence.flotchart.model.options.NumberAxisOptions;
import org.apache.commons.lang.ArrayUtils;

import java.util.Locale;

public class DataUtilTestCase extends TestCase
{
    private String[][] data = {
            new String[] { "1", "2", "3"  },
            new String[] { "4", "5", "6" }
    };

    private DataUtil dataUtil;

    private Locale locale;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        locale = Locale.getDefault();
        dataUtil = new DataUtil(new NumberAxisOptions(locale), new NumberAxisOptions(locale), DataUtil.DataModel.HORIZONTAL, false);
    }

    public void testRotateDataNinetyDegrees()
    {
        assertTrue(
                ArrayUtils.isEquals(
                        new String[][] {
                                new String[] { "4", "1" },
                                new String[] { "5", "2" },
                                new String[] { "6", "3" }
                        },
                        dataUtil.rotateNinetyDegreesClockWise(data)
                )
        );
    }

    public void testRotateDataHundredEightyDegrees()
    {
        assertTrue(
                ArrayUtils.isEquals(
                            new String[][] {
                                new String[] { "6", "5", "4"  },
                                new String[] { "3", "2", "1" }
                        },
                        dataUtil.rotateNinetyDegreesClockWise(
                                dataUtil.rotateNinetyDegreesClockWise(data)
                        )
                )
        );
    }

    public void testRotateDataTwoHundredSeventyDegrees()
    {
        assertTrue(
                ArrayUtils.isEquals(
                            new String[][] {
                                new String[] { "3", "6" },
                                new String[] { "2", "5" },
                                new String[] { "1", "4" }
                        },
                        dataUtil.rotateNinetyDegreesClockWise(
                                dataUtil.rotateNinetyDegreesClockWise(
                                        dataUtil.rotateNinetyDegreesClockWise(data)
                                )
                        )
                )
        );
    }

    public void testRotateDataThreeHundredSixtyDegrees()
    {
        assertTrue(
                ArrayUtils.isEquals(
                            new String[][] {
                                new String[] { "1", "2", "3"  },
                                new String[] { "4", "5", "6" }
                        },
                        dataUtil.rotateNinetyDegreesClockWise(
                                dataUtil.rotateNinetyDegreesClockWise(
                                        dataUtil.rotateNinetyDegreesClockWise(
                                                dataUtil.rotateNinetyDegreesClockWise(data)
                                        )
                                )
                        )
                )
        );
    }
}
