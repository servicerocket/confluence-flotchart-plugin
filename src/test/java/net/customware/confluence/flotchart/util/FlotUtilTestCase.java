package net.customware.confluence.flotchart.util;

import junit.framework.TestCase;


public class FlotUtilTestCase extends TestCase
{
    public void testConvertFlotFormatContainingTextToJavaFormat()
    {
        assertEquals("'This year in 'yyyy'/'MMM'/'d", FlotUtil.convertFlotTimeSeriesFormatToJavaDateFormat("This year in %y/%b/%d"));
    }

    public void testConvertFlotFormatContainingOnlyText()
    {
        assertEquals("'Hello world!'", FlotUtil.convertFlotTimeSeriesFormatToJavaDateFormat("Hello world!"));
    }

    public void testConvertFlotFormatContainingOnlyDateCharsAndWhiteSpace()
    {
        assertEquals("M' 'd', 'yyyy' 'HH':'mm':'ss", FlotUtil.convertFlotTimeSeriesFormatToJavaDateFormat("%m %d, %y %H:%M:%S"));
    }
}
