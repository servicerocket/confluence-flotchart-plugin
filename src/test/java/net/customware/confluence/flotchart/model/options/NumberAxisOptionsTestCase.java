package net.customware.confluence.flotchart.model.options;

import junit.framework.TestCase;

import java.util.Locale;

public class NumberAxisOptionsTestCase extends TestCase
{
    private NumberAxisOptions numberAxisOptions;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        numberAxisOptions = new NumberAxisOptions(new Locale("en", "US"));
    }

    public void testNumberAxisUnderStandsPercentageValuesByDefault()
    {
        assertEquals(20d, numberAxisOptions.toAxisValue("20%"));
    }

    public void testNumberAxisUnderStandsThousandSeparators()
    {
        assertEquals(1000d, numberAxisOptions.toAxisValue("1,000"));
    }

    public void testNumberAxisUnderStandsDecimalPoints()
    {
        assertEquals(1000.5, numberAxisOptions.toAxisValue("1,000.5"));
    }

    public void testNumberAxisUnderStandsNegativeValues()
    {
        assertEquals(-1000.5, numberAxisOptions.toAxisValue("-1,000.5"));
    }

    public void testNumberAxisUnderStandsDollarValues()
    {
        assertEquals(1000.5, numberAxisOptions.toAxisValue("$1,000.5"));
    }
}
