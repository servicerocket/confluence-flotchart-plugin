package net.customware.confluence.flotchart;

import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;
import junit.framework.TestCase;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Matchers;
import org.mockito.Mock;
import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class FlotChartMacroTestCase extends TestCase
{
    @Mock
    private I18NBeanFactory i18NBeanFactory;

    @Mock
    private I18NBean i18NBean;

    @Mock
    private LocaleManager localeManager;

    @Mock
    private VelocityHelperService velocityHelperService;

    private Map<String, String> macroParams;

    private Page pageToRenderOn;

    private FlotChartMacro flotChartMacro;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        when(i18NBeanFactory.getI18NBean(Matchers.<Locale>anyObject())).thenReturn(i18NBean);
        when(i18NBean.getText(anyString())).thenAnswer(
                new Answer<String>()
                {
                    public String answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return invocationOnMock.getArguments()[0].toString();
                    }
                }
        );
        when(i18NBean.getText(anyString(), (List) anyObject())).thenAnswer(
                new Answer<String>()
                {
                    public String answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return invocationOnMock.getArguments()[0].toString();
                    }
                }
        );
        when(localeManager.getLocale((User) anyObject())).thenReturn(Locale.getDefault());

        macroParams = new HashMap<String, String>();
        pageToRenderOn = new Page();
        pageToRenderOn.setSpace(new Space("TST"));
        
        when(velocityHelperService.createDefaultVelocityContext()).thenReturn(new HashMap<String, Object>());

        flotChartMacro = new TestFlotChartMacro();
    }

    @Override
    protected void tearDown() throws Exception
    {
        i18NBeanFactory = null;
        localeManager = null;
        i18NBean = null;
        velocityHelperService = null;
        super.tearDown();
    }

    //    public void testUnsupportedErrorRenderedInStaticRenderModes() throws MacroException
//    {
//        Set<String> staticRenderModes = new HashSet<String>(
//                Arrays.asList(
//                        RenderContext.EMAIL,
//                        RenderContext.FEED,
//                        RenderContext.HTML_EXPORT,
//                        RenderContext.PDF,
//                        RenderContext.WORD
//                )
//        );
//
//        PageContext pageContext = pageToRenderOn.toPageContext();
//
//        for (String renderMode : staticRenderModes)
//        {
//            pageContext.setOutputType(renderMode);
//            assertEquals("<div class=\"error\"><span class=\"error\">flotchart.error.unsupportedrendermode</span> </div>", flotChartMacro.execute(macroParams, "", pageContext));
//        }
//    }

    public void testErrorRenderedIfChartTypeNotSpecified() throws MacroException
    {
        assertEquals("<div class=\"error\"><span class=\"error\">flotchart.error.invalidcharttype</span> </div>", flotChartMacro.execute(macroParams, "", pageToRenderOn.toPageContext()));
    }

    public void testErrorRenderedIfChartTypeInvalid() throws MacroException
    {
        macroParams.put("type", "invalid");
        assertEquals("<div class=\"error\"><span class=\"error\">flotchart.error.invalidcharttype</span> </div>", flotChartMacro.execute(macroParams, "", pageToRenderOn.toPageContext()));
    }

    public void testErrorRenderedIfSeriesColorIsNotInHexFormat() throws MacroException
    {
        macroParams.put("type", "line");
        macroParams.put("colors", "invalid");

        assertEquals("<div class=\"error\"><span class=\"error\">flotchart.error.invalidcolor</span> </div>", flotChartMacro.execute(macroParams, "", pageToRenderOn.toPageContext()));
    }

    public void testErrorRenderedIfTablesCannotBeFoundInMacroBody() throws MacroException
    {
        macroParams.put("type", "line");
        assertEquals("<div class=\"error\"><span class=\"error\">flotchart.error.cantfindtables</span> </div>", flotChartMacro.execute(macroParams, "", pageToRenderOn.toPageContext()));
    }

    private String getClassPathResourceAsString(String classPathResource, String charset) throws IOException
    {
        Reader in = null;
        Writer out = null;

        try
        {
            in = new InputStreamReader(getClass().getClassLoader().getResourceAsStream(classPathResource), StringUtils.defaultString(charset, "UTF-8"));
            out = new StringWriter();

            IOUtils.copy(in, out);

            return out.toString();
        }
        finally
        {
            IOUtils.closeQuietly(out);
            IOUtils.closeQuietly(in);
        }
    }

    public void testErrorRenderedIfTimeSeriesFormatIsInvalid() throws MacroException, IOException
    {
        macroParams.put("type", "line");
        macroParams.put("timeSeriesFormat", "invalid");
        assertEquals(
                "<div class=\"error\"><span class=\"error\">flotchart.error.invaliddateformat</span> </div>",
                flotChartMacro.execute(
                        macroParams,
                        getClassPathResourceAsString("net/customware/confluence/flotchart/min-max-table.html", "UTF-8"),
                        pageToRenderOn.toPageContext()));
    }

    public void testErrorRenderedIfNumberFormatIsInvalid() throws MacroException, IOException
    {
        macroParams.put("type", "line");
        macroParams.put("numberFormat", "%%");
        assertEquals(
                "<div class=\"error\"><span class=\"error\">flotchart.error.invalidnumberformat</span> </div>",
                flotChartMacro.execute(
                        macroParams,
                        getClassPathResourceAsString("net/customware/confluence/flotchart/min-max-table.html", "UTF-8"),
                        pageToRenderOn.toPageContext()));
    }

    public void testDefaultChartDimensionsUsedIfNoneSpecified() throws MacroException, IOException
    {
        macroParams.put("type", "line");

        assertEquals(
                "",
                new TestFlotChartMacro() {
                    @Override
                    String renderChart(Map<String, Object> macroVelocityContext)
                    {
                        assertEquals("384px", macroVelocityContext.get("width"));
                        assertEquals("256px", macroVelocityContext.get("height"));
                        return "";
                    }
                }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/min-max-table.html", "UTF-8"), pageToRenderOn.toPageContext())
        );
    }

    public void testUseCustomChartDimensions() throws MacroException, IOException
    {
        final String customWidth = "100";
        final String customHeight = "50";

        macroParams.put("type", "line");
        macroParams.put("width", customWidth);
        macroParams.put("height", customHeight);

        assertEquals(
                "",
                new TestFlotChartMacro() {
                    @Override
                    String renderChart(Map<String, Object> macroVelocityContext)
                    {
                        assertEquals(customWidth + "px", macroVelocityContext.get("width"));
                        assertEquals(customHeight + "px", macroVelocityContext.get("height"));
                        return "";
                    }
                }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/min-max-table.html", "UTF-8"), pageToRenderOn.toPageContext())
        );
    }

    public void testShowSeriesToggleDefaultsToFalse() throws MacroException, IOException
    {
        macroParams.put("type", "line");

        assertEquals(
                "",
                new TestFlotChartMacro() {
                    @Override
                    String renderChart(Map<String, Object> macroVelocityContext)
                    {
                        assertFalse((Boolean) macroVelocityContext.get("showSeriesToggle"));
                        return "";
                    }
                }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/min-max-table.html", "UTF-8"), pageToRenderOn.toPageContext())
        );
    }

    public void testShowShowSeriesToggle() throws MacroException, IOException
    {
        macroParams.put("type", "line");
        macroParams.put("showSeriesToggle", "true");

        assertEquals(
                "",
                new TestFlotChartMacro() {
                    @Override
                    String renderChart(Map<String, Object> macroVelocityContext)
                    {
                        assertTrue((Boolean) macroVelocityContext.get("showSeriesToggle"));
                        return "";
                    }
                }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/min-max-table.html", "UTF-8"), pageToRenderOn.toPageContext())
        );
    }

    public void testEnableShowDataPoint() throws MacroException, IOException, JSONException
    {
        macroParams.put("type", "line");
        macroParams.put("showShapes", "true");
        macroParams.put("showDataPointValue", "true");

        String chartJsonString = new TestFlotChartMacro()
        {
            @Override
            String renderChart(Map<String, Object> macroVelocityContext)
            {
                return (String) macroVelocityContext.get("chartJson");
            }
        }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/min-max-table.html", "UTF-8"), pageToRenderOn.toPageContext());

        JSONObject chartJson = new JSONObject(chartJsonString);
        assertTrue(chartJson.has("options"));

        JSONObject chartOptionsJson = chartJson.getJSONObject("options");
        assertTrue(chartOptionsJson.has("grid"));

        JSONObject chartGridOptionsJson = chartOptionsJson.getJSONObject("grid");
        assertTrue(chartGridOptionsJson.getBoolean("hoverable"));
    }

    public void testShowDataPointNotEnabledWhenShowShapesIsFalse() throws MacroException, IOException, JSONException
    {
        macroParams.put("type", "line");
        macroParams.put("showShapes", "false");
        macroParams.put("showDataPointValue", "true");

        String chartJsonString = new TestFlotChartMacro()
        {
            @Override
            String renderChart(Map<String, Object> macroVelocityContext)
            {
                return (String) macroVelocityContext.get("chartJson");
            }
        }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/min-max-table.html", "UTF-8"), pageToRenderOn.toPageContext());

        JSONObject chartJson = new JSONObject(chartJsonString);
        assertTrue(chartJson.has("options"));

        JSONObject chartOptionsJson = chartJson.getJSONObject("options");
        assertTrue(chartOptionsJson.has("grid"));

        JSONObject chartGridOptionsJson = chartOptionsJson.getJSONObject("grid");
        assertFalse(chartGridOptionsJson.getBoolean("hoverable"));
    }

    public void testZoomDefaultsToTrue() throws MacroException, IOException
    {
        macroParams.put("type", "line");

        assertEquals(
                "",
                new TestFlotChartMacro() {
                    @Override
                    String renderChart(Map<String, Object> macroVelocityContext)
                    {
                        assertTrue((Boolean) macroVelocityContext.get("zoom"));
                        return "";
                    }
                }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/min-max-table.html", "UTF-8"), pageToRenderOn.toPageContext())
        );
    }

    public void testEnableZoom() throws MacroException, IOException
    {
        macroParams.put("type", "line");
        macroParams.put("enableZoom", "true");

        assertEquals(
                "",
                new TestFlotChartMacro() {
                    @Override
                    String renderChart(Map<String, Object> macroVelocityContext)
                    {
                        assertTrue((Boolean) macroVelocityContext.get("zoom"));
                        return "";
                    }
                }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/min-max-table.html", "UTF-8"), pageToRenderOn.toPageContext())
        );
    }


    private void assertLegend(JSONObject legendObject, boolean show, String position) throws JSONException
    {
        assertEquals(show, legendObject.getBoolean("show"));
        assertEquals(position, legendObject.getString("position"));
    }

    public void testLegendShownAtNorthEastByDefault() throws MacroException, IOException, JSONException
    {
        macroParams.put("type", "line");

        String chartJson = new TestFlotChartMacro() {
            @Override
            String renderChart(Map<String, Object> macroVelocityContext)
            {
                return (String) macroVelocityContext.get("chartJson");
            }
        }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/min-max-table.html", "UTF-8"), pageToRenderOn.toPageContext());

        JSONObject chartObject = new JSONObject(chartJson);
        assertLegend(chartObject.getJSONObject("options").getJSONObject("legend"), true, "ne");
    }

    public void testShowLegendAtCustomPosition() throws MacroException, IOException, JSONException
    {
        macroParams.put("type", "line");
        macroParams.put("legendPosition", "se");

        String chartJson = new TestFlotChartMacro() {
            @Override
            String renderChart(Map<String, Object> macroVelocityContext)
            {
                return (String) macroVelocityContext.get("chartJson");
            }
        }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/min-max-table.html", "UTF-8"), pageToRenderOn.toPageContext());

        JSONObject chartObject = new JSONObject(chartJson);
        assertLegend(chartObject.getJSONObject("options").getJSONObject("legend"), true, "se");
    }

    public void testHideLegend() throws MacroException, IOException, JSONException
    {
        macroParams.put("type", "line");
        macroParams.put("showLegend", "false");

        String chartJson = new TestFlotChartMacro() {
            @Override
            String renderChart(Map<String, Object> macroVelocityContext)
            {
                return (String) macroVelocityContext.get("chartJson");
            }
        }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/min-max-table.html", "UTF-8"), pageToRenderOn.toPageContext());

        JSONObject chartObject = new JSONObject(chartJson);
        assertLegend(chartObject.getJSONObject("options").getJSONObject("legend"), false, "ne");
    }

    private void assertSeries(JSONObject seriesObject, String label, Number[][] dataSeries, String color) throws JSONException
    {
        assertEquals(label, seriesObject.getString("label"));

        JSONArray dataArray = seriesObject.getJSONArray("data");
        assertEquals(dataSeries.length, dataArray.length());

        for (int i = 0; i < dataSeries.length; ++i)
        {
            JSONArray dataPoint = dataArray.getJSONArray(i);

            assertEquals(dataSeries[i].length, dataPoint.length());

            for (int j = 0; j < dataSeries[i].length; ++j)
                assertEquals(dataSeries[i][j], dataPoint.getDouble(j));
        }

        if (null != color)
            assertEquals(color, seriesObject.getString("color"));
        else
            assertFalse(seriesObject.has("color"));
    }

    private void assertSeries(JSONObject seriesObject, String label, Number value, String color) throws JSONException
    {
        assertEquals(label, seriesObject.getString("label"));

        assertEquals(value, seriesObject.getDouble("data"));

        if (null != color)
            assertEquals(color, seriesObject.getString("color"));
        else
            assertFalse(seriesObject.has("color"));
    }

    private void assertLine(JSONObject lineObject, boolean show, Number lineWidth, Number fill, String fillColor) throws JSONException
    {
        assertEquals(show, lineObject.getBoolean("show"));

        if (null != lineWidth)
            assertEquals(lineWidth, lineObject.getDouble("lineWidth"));
        else
            assertFalse(lineObject.has("lineWidth"));

        if (null != fill)
            assertEquals(fill, lineObject.getDouble("fill"));
        else
            assertFalse(lineObject.has("fill"));

        if (null != fillColor)
            assertEquals(fillColor, lineObject.getString("fillColor"));
        else
            assertFalse(lineObject.has("fillColor"));
    }

    private void assertPoint(JSONObject pointsObject, boolean show, Number lineWidth, Number fill, String fillColor, Number radius) throws JSONException
    {
        assertLine(pointsObject, show, lineWidth, fill, fillColor);
        if (null != radius)
            assertEquals(radius, pointsObject.getDouble("radius"));
        else
            assertFalse(pointsObject.has("radius"));
    }

    private void assertBar(JSONObject barsObject, boolean show, boolean horizontal, Number lineWidth, Number fill, String fillColor, Number barWidth, String align) throws JSONException
    {
        assertLine(barsObject, show, lineWidth, fill, fillColor);

        assertEquals(horizontal, barsObject.getBoolean("horizontal"));

        if (null != barWidth)
            assertEquals(barWidth, barsObject.getDouble("barWidth"));
        else
            assertFalse(barsObject.has("barWidth"));

        if (null != align)
            assertEquals(align, barsObject.getString("align"));
        else
            assertFalse(barsObject.has("align"));
    }

    private void assertPie(JSONObject pieObject, boolean show, Number lineWidth, Number fill, String fillColor, Number radius, Number combineThreshold, String combinedSectionLabel, boolean showLabels) throws JSONException
    {
        assertLine(pieObject, show, lineWidth, fill, fillColor);
        if (null != radius)
            assertEquals(radius, pieObject.getDouble("radius"));
        else
            assertFalse(pieObject.has("radius"));

        if (null != combineThreshold)
        {
            assertTrue(pieObject.has("combine"));
            assertEquals(combineThreshold, pieObject.getJSONObject("combine").getDouble("threshold"));
        }
        else
        {
            if (pieObject.has("combine"))
                assertFalse(pieObject.getJSONObject("combine").has("threshold"));
        }

        if (null != combinedSectionLabel)
        {
            assertTrue(pieObject.has("combine"));
            assertEquals(combinedSectionLabel, pieObject.getJSONObject("combine").getString("label"));
        }
        else
        {
            if (pieObject.has("combine"))
                assertFalse(pieObject.getJSONObject("combine").has("label"));
        }

        if (!showLabels)
            assertEquals(showLabels, pieObject.getJSONObject("label").getBoolean("show"));
        else
        {
            if (pieObject.has("label"))
                assertTrue(pieObject.getJSONObject("label").getBoolean("show"));
        }
    }

    private void assertAxis(JSONObject axisObject, String mode, Number min, Number max, Number minTickSize, LinkedHashMap<Number, String> ticks) throws JSONException
    {
        if (null != mode)
            assertEquals(mode, axisObject.getString("mode"));
        else
            assertFalse(axisObject.has("mode"));

        if (null != min)
            assertEquals(min, axisObject.getDouble("min"));
        else
            assertFalse(axisObject.has("min"));

        if (null != max)
            assertEquals(max, axisObject.getDouble("max"));
        else
            assertFalse(axisObject.has("max"));

        if (null != minTickSize)
            assertEquals(minTickSize, axisObject.getDouble("minTickSize"));
        else
            assertFalse(axisObject.has("minTickSize"));

        if (null != ticks)
        {
            List<Number> tickKeys = new ArrayList<Number>(ticks.keySet());
            JSONArray ticksArray = axisObject.getJSONArray("ticks");

            assertEquals(tickKeys.size(), ticksArray.length());

            for (int i = 0, j = tickKeys.size(); i < j; ++i)
            {
                JSONArray tickJson = ticksArray.getJSONArray(i);

                assertEquals(tickKeys.get(i), tickJson.getDouble(0));
                assertEquals(ticks.get(tickKeys.get(i)), tickJson.getString(1));
            }
        }
    }

    public void testSetCustomColorsToSeries() throws MacroException, IOException, JSONException
    {
        macroParams.put("type", "line");
        macroParams.put("colors", "#00ff00,#ff0000");

        String chartJson = new TestFlotChartMacro() {
            @Override
            String renderChart(Map<String, Object> macroVelocityContext)
            {
                return (String) macroVelocityContext.get("chartJson");
            }
        }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/min-max-table.html", "UTF-8"), pageToRenderOn.toPageContext());

        JSONObject chartObject = new JSONObject(chartJson);
        JSONArray seriesArray = chartObject.getJSONArray("series");

        assertEquals(2, seriesArray.length());

        assertSeries(
                seriesArray.getJSONObject(0),
                "Max",
                new Number[][] {
                        new Number[] { 0.0, 37.5 },
                        new Number[] { 1.0, 32.7 },
                        new Number[] { 2.0, 28.0 },
                        new Number[] { 3.0, 45.3 }
                },
                "#00ff00"
        );

        assertSeries(
                seriesArray.getJSONObject(1),
                "Min",
                new Number[][] {
                        new Number[] { 0.0, 31.3 },
                        new Number[] { 1.0, 26.8 },
                        new Number[] { 2.0, 25.1 },
                        new Number[] { 3.0, 18.7 }
                },
                "#ff0000"
        );

        LinkedHashMap<Number, String> ticksMap = new LinkedHashMap<Number, String>();

        ticksMap.put(0.0, "January");
        ticksMap.put(1.0, "February");
        ticksMap.put(2.0, "March");
        ticksMap.put(3.0, "April");

        JSONObject plotOptions = chartObject.getJSONObject("options");

        assertAxis(plotOptions.getJSONObject("xaxis"), null, null, null, 1.0, ticksMap);

        assertLine(plotOptions.getJSONObject("series").getJSONObject("lines"), true, null, null, null);
        assertPoint(plotOptions.getJSONObject("series").getJSONObject("points"), true, null, null, null, null);
        assertFalse(plotOptions.getJSONObject("series").has("bars"));
        assertFalse(plotOptions.getJSONObject("series").has("pie"));
    }

    public void testGenerateLineChartWithNonScalableXaxis() throws MacroException, IOException, JSONException
    {
        macroParams.put("type", "line");

        String chartJson = new TestFlotChartMacro() {
            @Override
            String renderChart(Map<String, Object> macroVelocityContext)
            {
                return (String) macroVelocityContext.get("chartJson");
            }
        }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/min-max-table.html", "UTF-8"), pageToRenderOn.toPageContext());

        JSONObject chartObject = new JSONObject(chartJson);
        JSONArray seriesArray = chartObject.getJSONArray("series");

        assertEquals(2, seriesArray.length());

        assertSeries(
                seriesArray.getJSONObject(0),
                "Max",
                new Number[][] {
                        new Number[] { 0.0, 37.5 },
                        new Number[] { 1.0, 32.7 },
                        new Number[] { 2.0, 28.0 },
                        new Number[] { 3.0, 45.3 }
                },
                "#edc240"
        );

        assertSeries(
                seriesArray.getJSONObject(1),
                "Min",
                new Number[][] {
                        new Number[] { 0.0, 31.3 },
                        new Number[] { 1.0, 26.8 },
                        new Number[] { 2.0, 25.1 },
                        new Number[] { 3.0, 18.7 }
                },
                "#afd8f8"
        );

        LinkedHashMap<Number, String> ticksMap = new LinkedHashMap<Number, String>();

        ticksMap.put(0.0, "January");
        ticksMap.put(1.0, "February");
        ticksMap.put(2.0, "March");
        ticksMap.put(3.0, "April");

        JSONObject plotOptions = chartObject.getJSONObject("options");

        assertAxis(plotOptions.getJSONObject("xaxis"), null, null, null, 1.0, ticksMap);


        assertLine(plotOptions.getJSONObject("series").getJSONObject("lines"), true, null, null, null);
        assertPoint(plotOptions.getJSONObject("series").getJSONObject("points"), true, null, null, null, null);
        assertFalse(plotOptions.getJSONObject("series").has("bars"));
        assertFalse(plotOptions.getJSONObject("series").has("pie"));
    }

    public void testGenerateLineChartWithCustomNumberFormat() throws MacroException, IOException, JSONException
    {
        macroParams.put("type", "line");
        macroParams.put("numberFormat", "#,###.#");

        String chartJson = new TestFlotChartMacro() {
            @Override
            String renderChart(Map<String, Object> macroVelocityContext)
            {
                return (String) macroVelocityContext.get("chartJson");
            }
        }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/min-max-table-with-thousand-separator.html", "UTF-8"), pageToRenderOn.toPageContext());

        JSONObject chartObject = new JSONObject(chartJson);
        JSONArray seriesArray = chartObject.getJSONArray("series");

        assertEquals(2, seriesArray.length());

        assertSeries(
                seriesArray.getJSONObject(0),
                "Max",
                new Number[][] {
                        new Number[] { 0.0, 1337.5 },
                        new Number[] { 1.0, 1332.7 },
                        new Number[] { 2.0, 1328.0 },
                        new Number[] { 3.0, 1345.3 }
                },
                "#edc240"
        );

        assertSeries(
                seriesArray.getJSONObject(1),
                "Min",
                new Number[][] {
                        new Number[] { 0.0, 1331.3 },
                        new Number[] { 1.0, 1326.8 },
                        new Number[] { 2.0, 1325.1 },
                        new Number[] { 3.0, 1318.7 }
                },
                "#afd8f8"
        );

        LinkedHashMap<Number, String> ticksMap = new LinkedHashMap<Number, String>();

        ticksMap.put(0.0, "January");
        ticksMap.put(1.0, "February");
        ticksMap.put(2.0, "March");
        ticksMap.put(3.0, "April");

        JSONObject plotOptions = chartObject.getJSONObject("options");

        assertAxis(plotOptions.getJSONObject("xaxis"), null, null, null, 1.0, ticksMap);


        assertLine(plotOptions.getJSONObject("series").getJSONObject("lines"), true, null, null, null);
        assertPoint(plotOptions.getJSONObject("series").getJSONObject("points"), true, null, null, null, null);
        assertFalse(plotOptions.getJSONObject("series").has("bars"));
        assertFalse(plotOptions.getJSONObject("series").has("pie"));
    }

    public void testGenerateAreaChartSetsTheFillColor() throws MacroException, IOException, JSONException
    {
        macroParams.put("type", "area");

        String chartJson = new TestFlotChartMacro() {
            @Override
            String renderChart(Map<String, Object> macroVelocityContext)
            {
                return (String) macroVelocityContext.get("chartJson");
            }
        }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/min-max-table.html", "UTF-8"), pageToRenderOn.toPageContext());

        JSONObject chartObject = new JSONObject(chartJson);
        JSONArray seriesArray = chartObject.getJSONArray("series");

        assertEquals(2, seriesArray.length());

        JSONObject plotOptions = chartObject.getJSONObject("options");

        assertLine(plotOptions.getJSONObject("series").getJSONObject("lines"), true, null, 0.5, null);
        assertPoint(plotOptions.getJSONObject("series").getJSONObject("points"), true, null, null, null, null);
        assertFalse(plotOptions.getJSONObject("series").has("bars"));
        assertFalse(plotOptions.getJSONObject("series").has("pie"));
    }

    public void testGenerateAreaChartWithCustomOpacity() throws MacroException, IOException, JSONException
    {
        double opacity = 0.7;

        macroParams.put("type", "area");
        macroParams.put("opacity", String.valueOf(opacity));

        String chartJson = new TestFlotChartMacro() {
            @Override
            String renderChart(Map<String, Object> macroVelocityContext)
            {
                return (String) macroVelocityContext.get("chartJson");
            }
        }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/min-max-table.html", "UTF-8"), pageToRenderOn.toPageContext());

        JSONObject chartObject = new JSONObject(chartJson);
        JSONArray seriesArray = chartObject.getJSONArray("series");

        assertEquals(2, seriesArray.length());


        JSONObject plotOptions = chartObject.getJSONObject("options");

        assertLine(plotOptions.getJSONObject("series").getJSONObject("lines"), true, null, opacity, null);
        assertPoint(plotOptions.getJSONObject("series").getJSONObject("points"), true, null, null, null, null);
        assertFalse(plotOptions.getJSONObject("series").has("bars"));
        assertFalse(plotOptions.getJSONObject("series").has("pie"));
    }

    public void testGenerateBarChartDoesNotContainLinesAndPointsOptions() throws MacroException, IOException, JSONException
    {
        macroParams.put("type", "bar");

        String chartJson = new TestFlotChartMacro() {
            @Override
            String renderChart(Map<String, Object> macroVelocityContext)
            {
                return (String) macroVelocityContext.get("chartJson");
            }
        }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/min-max-table.html", "UTF-8"), pageToRenderOn.toPageContext());

        JSONObject chartObject = new JSONObject(chartJson);
        JSONArray seriesArray = chartObject.getJSONArray("series");

        assertEquals(2, seriesArray.length());


        JSONObject plotOptions = chartObject.getJSONObject("options");

        assertBar(plotOptions.getJSONObject("series").getJSONObject("bars"), true, false, null, null, null, 1.0, "center");
        assertFalse(plotOptions.getJSONObject("series").has("points"));
        assertFalse(plotOptions.getJSONObject("series").has("lines"));
        assertFalse(plotOptions.getJSONObject("series").has("pie"));

        LinkedHashMap<Number, String> ticksMap = new LinkedHashMap<Number, String>();

        ticksMap.put(0.0, "January");
        ticksMap.put(1.0, "February");
        ticksMap.put(2.0, "March");
        ticksMap.put(3.0, "April");

        assertAxis(plotOptions.getJSONObject("xaxis"), null, null, null, 1.0, ticksMap);
    }

    public void testGenerateBarChartWithCustomBarWidth() throws MacroException, IOException, JSONException
    {
        macroParams.put("type", "bar");
        macroParams.put("barWidth", "0.5");

        String chartJson = new TestFlotChartMacro() {
            @Override
            String renderChart(Map<String, Object> macroVelocityContext)
            {
                return (String) macroVelocityContext.get("chartJson");
            }
        }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/min-max-table.html", "UTF-8"), pageToRenderOn.toPageContext());

        JSONObject chartObject = new JSONObject(chartJson);
        JSONArray seriesArray = chartObject.getJSONArray("series");

        assertEquals(2, seriesArray.length());


        JSONObject plotOptions = chartObject.getJSONObject("options");

        assertBar(plotOptions.getJSONObject("series").getJSONObject("bars"), true, false, null, null, null, 0.5, "center");
        assertFalse(plotOptions.getJSONObject("series").has("points"));
        assertFalse(plotOptions.getJSONObject("series").has("lines"));
        assertFalse(plotOptions.getJSONObject("series").has("pie"));


        LinkedHashMap<Number, String> ticksMap = new LinkedHashMap<Number, String>();

        ticksMap.put(0.0, "January");
        ticksMap.put(1.0, "February");
        ticksMap.put(2.0, "March");
        ticksMap.put(3.0, "April");

        assertAxis(plotOptions.getJSONObject("xaxis"), null, null, null, 1.0, ticksMap);
    }

    public void testGenerateLineChartWithScalableXaxisAndVerticallyOrientedData() throws MacroException, IOException, JSONException
    {
        macroParams.put("type", "line");
        macroParams.put("timeSeriesFormat", "MMMM");
        macroParams.put("dataOrientation", "vertical");

        String chartJson = new TestFlotChartMacro() {
            @Override
            String renderChart(Map<String, Object> macroVelocityContext)
            {
                return (String) macroVelocityContext.get("chartJson");
            }
        }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/min-avg-max-temperature-table.html", "UTF-8"), pageToRenderOn.toPageContext());

        JSONObject chartObject = new JSONObject(chartJson);
        JSONArray seriesArray = chartObject.getJSONArray("series");

        assertEquals(3, seriesArray.length());

        assertSeries(
                seriesArray.getJSONObject(0),
                "Max Temperature",
                new Number[][] {
                        new Number[] { -27000000.0, 25.5 },
                        new Number[] { 2651400000.0, 32.4 },
                        new Number[] { 5070600000.0, 44.6 }
                },
                "#edc240"
        );


        assertSeries(
                seriesArray.getJSONObject(1),
                "Min Temperature",
                new Number[][] {
                        new Number[] { -27000000.0, 6.3 },
                        new Number[] { 2651400000.0, 12.8 },
                        new Number[] { 5070600000.0, 24.5 }
                },
                "#afd8f8"
        );

        assertSeries(
                seriesArray.getJSONObject(2),
                "Average Temperature",
                new Number[][] {
                        new Number[] { -27000000.0, 15.9 },
                        new Number[] { 2651400000.0, 22.6 },
                        new Number[] { 5070600000.0, 34.6 }
                },
                "#cb4b4b"
        );



        JSONObject plotOptions = chartObject.getJSONObject("options");

        assertLine(plotOptions.getJSONObject("series").getJSONObject("lines"), true, null, null, null);
        assertPoint(plotOptions.getJSONObject("series").getJSONObject("points"), true, null, null, null, null);
        assertFalse(plotOptions.getJSONObject("series").has("bars"));
        assertFalse(plotOptions.getJSONObject("series").has("pie"));

        assertAxis(plotOptions.getJSONObject("xaxis"), "time", null, null, null, null);
    }

    public void testGeneratePieChart() throws MacroException, IOException, JSONException
    {
        macroParams.put("type", "pie");
        macroParams.put("dataOrientation", "vertical");

        String chartJson = new TestFlotChartMacro() {
            @Override
            String renderChart(Map<String, Object> macroVelocityContext)
            {
                return (String) macroVelocityContext.get("chartJson");
            }
        }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/party-mascots-table.html", "UTF-8"), pageToRenderOn.toPageContext());

        JSONObject chartObject = new JSONObject(chartJson);
        JSONArray seriesArray = chartObject.getJSONArray("series");

        assertEquals(4, seriesArray.length());

        assertSeries(
                seriesArray.getJSONObject(0),
                "Democrat",
                40.0,
                "#edc240"
        );

        assertSeries(
                seriesArray.getJSONObject(1),
                "Republican",
                40.0,
                "#afd8f8"
        );

        assertSeries(
                seriesArray.getJSONObject(2),
                "Independent",
                10.0,
                "#cb4b4b"
        );

        assertSeries(
                seriesArray.getJSONObject(3),
                "Secret",
                10.0,
                "#4da74d"
        );

        assertPie(chartObject.getJSONObject("options").getJSONObject("series").getJSONObject("pie"),
                true, null, null, null, null, 0.03, null, true
        );
    }



    public void testErrorReturnedWhebGeneratingPieChartWithAnEmtyDataSetOrOneThatCouldNotBeReadIntoGrid() throws MacroException, IOException, JSONException
    {
        macroParams.put("type", "pie");
        macroParams.put("dataOrientation", "vertical");

        String result = flotChartMacro.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/empty-mascots-table.html", "UTF-8"), pageToRenderOn.toPageContext());

        assertEquals("<div class=\"error\"><span class=\"error\">flotchart.error.emptypie</span> </div>", result);
    }

    public void testGeneratePieChartWithCustomRadius() throws MacroException, IOException, JSONException
    {
        macroParams.put("type", "pie");
        macroParams.put("dataOrientation", "vertical");
        macroParams.put("pieRadius", "0.5");

        String chartJson = new TestFlotChartMacro() {
            @Override
            String renderChart(Map<String, Object> macroVelocityContext)
            {
                return (String) macroVelocityContext.get("chartJson");
            }
        }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/party-mascots-table.html", "UTF-8"), pageToRenderOn.toPageContext());

        JSONObject chartObject = new JSONObject(chartJson);
        JSONArray seriesArray = chartObject.getJSONArray("series");

        assertEquals(4, seriesArray.length());

        assertPie(chartObject.getJSONObject("options").getJSONObject("series").getJSONObject("pie"),
                true, null, null, null, 0.5, 0.03, null, true
        );
    }

    public void testGeneratePieChartWithoutSectionLabels() throws MacroException, IOException, JSONException
    {
        macroParams.put("type", "pie");
        macroParams.put("dataOrientation", "vertical");
        macroParams.put("showLegend", "false");

        String chartJson = new TestFlotChartMacro() {
            @Override
            String renderChart(Map<String, Object> macroVelocityContext)
            {
                return (String) macroVelocityContext.get("chartJson");
            }
        }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/party-mascots-table.html", "UTF-8"), pageToRenderOn.toPageContext());

        JSONObject chartObject = new JSONObject(chartJson);
        JSONArray seriesArray = chartObject.getJSONArray("series");

        assertEquals(4, seriesArray.length());

        assertPie(chartObject.getJSONObject("options").getJSONObject("series").getJSONObject("pie"),
                true, null, null, null, null, 0.03, null, false
        );
    }

    public void testGeneratePieChartWithCustomCombineThreshold() throws MacroException, IOException, JSONException
    {
        macroParams.put("type", "pie");
        macroParams.put("dataOrientation", "vertical");
        macroParams.put("combineThreshold", "0.1");

        String chartJson = new TestFlotChartMacro() {
            @Override
            String renderChart(Map<String, Object> macroVelocityContext)
            {
                return (String) macroVelocityContext.get("chartJson");
            }
        }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/party-mascots-table.html", "UTF-8"), pageToRenderOn.toPageContext());

        JSONObject chartObject = new JSONObject(chartJson);
        JSONArray seriesArray = chartObject.getJSONArray("series");

        assertEquals(4, seriesArray.length());

        assertPie(chartObject.getJSONObject("options").getJSONObject("series").getJSONObject("pie"),
                true, null, null, null, null, 0.1, null, true
        );
    }

    public void testGeneratePieChartWithCustomOtherSegmentLabel() throws MacroException, IOException, JSONException
    {
        macroParams.put("type", "pie");
        macroParams.put("dataOrientation", "vertical");
        macroParams.put("otherLabel", "customOtherLabel");

        String chartJson = new TestFlotChartMacro() {
            @Override
            String renderChart(Map<String, Object> macroVelocityContext)
            {
                return (String) macroVelocityContext.get("chartJson");
            }
        }.execute(macroParams, getClassPathResourceAsString("net/customware/confluence/flotchart/party-mascots-table.html", "UTF-8"), pageToRenderOn.toPageContext());

        JSONObject chartObject = new JSONObject(chartJson);
        JSONArray seriesArray = chartObject.getJSONArray("series");

        assertEquals(4, seriesArray.length());

        assertPie(chartObject.getJSONObject("options").getJSONObject("series").getJSONObject("pie"),
                true, null, null, null, null, 0.03, "customOtherLabel", true
        );
    }

    private class TestFlotChartMacro extends FlotChartMacro
    {
        private TestFlotChartMacro()
        {
            super(i18NBeanFactory, localeManager, null, velocityHelperService);
        }
    }

}
