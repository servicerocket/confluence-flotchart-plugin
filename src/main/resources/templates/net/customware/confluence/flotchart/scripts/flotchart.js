
jQuery(function($) {
    var flotchart = {
        chartObjects : [],
        plots : [],

        getFieldset : function(flotChartDiv) {
            return flotChartDiv.children("fieldset.hidden");
        },

        getParams : function(fieldSet) {
            var params = {};

            fieldSet.find("input").each(function() {
                if (params[this.name]) {
                    var paramValues = [];
                    paramValues.push(params[this.name]);
                    paramValues.push(this.value);

                    params[this.name] = paramValues;

                } else {
                    params[this.name] = this.value;
                }
            });

            return params;
        },

        getFlotChartDivIndex : function(flotChartDiv) {
            return $.data(flotChartDiv.get(0), "elementIndex");
        },

        getChartData : function(flotChartDiv) {
            var chartData = null;
            var index = this.getFlotChartDivIndex(flotChartDiv);

            if (index < this.chartObjects.length) {
                chartData = this.chartObjects[index];
            } else {
                var params = this.getParams(this.getFieldset(flotChartDiv));

                if (params["chartData"]) {
                    chartData = eval("(" + params["chartData"] + ")");

                    if (params["zoom"] == "true") {
                        chartData["options"].zoom = {
                            interactive : true
                        };
                        chartData["options"].pan = {
                            interactive : true
                        };
                    }

                    this.chartObjects[index] = chartData;
                }
            }

            return chartData;
        },

        getResetButtonContainer : function(flotChartDiv) {
            return flotChartDiv.children("div.plugin_flotchart_resetzoom");
        },

        getResetButton : function(flotChartDiv) {
            return this.getResetButtonContainer(flotChartDiv).children("input[type='button']");
        },

        initResetZoomButton : function(flotChartDiv) {
            this.getResetButton(flotChartDiv).click(function() {
                flotchart.setPlot(flotChartDiv, null);
                flotchart.plotChart(flotChartDiv);
                $(this).parent().hide();
            });
        },

        getSeriesToggleContainer : function(flotChartDiv) {
            return flotChartDiv.find("div.plugin_flotchart_seriestoggler");
        },

        initDataSeries : function(flotChartDiv) {
            var seriesToggler = this.getSeriesToggleContainer(flotChartDiv);
            var chartData = this.getChartData(flotChartDiv);

            if (chartData && chartData["series"]) {
                var seriesArray = chartData["series"];
                var fieldSet = seriesToggler.html("<fieldset/>").children("fieldset");

                fieldSet.addClass("hidden");

                for (var i = 0; i < seriesArray.length; ++i) {
                    var aSeries = seriesArray[i];
                    var checkBoxContainer = seriesToggler.append("<div/>").children("div:last");
                    var checkBox = checkBoxContainer.html("<input type='checkbox'/>").children("input");

                    checkBox.attr("value", i.toString());
                    checkBox.attr("checked", "checked");
                    checkBox.click(function() {
                        flotchart.plotChart(flotChartDiv);
                    });

                    var checkBoxLabel = $(document.createElement("span"));
                    checkBoxLabel.html(aSeries["label"]);

                    checkBoxContainer.append(checkBoxLabel);
                }
            }

            var params = this.getParams(this.getFieldset(flotChartDiv));
            if (params["showSeriesToggle"] == "true")
                seriesToggler.parent().show();
        },

        getDrawRegion : function(flotChartDiv) {
            return flotChartDiv.children("div.plugin_flotchart_drawregion");
        },

        getEnabledSeries : function(flotChartDiv) {
            var arrayOfSeriesToDraw = [];
            var seriesToggleContainer = flotchart.getSeriesToggleContainer(flotChartDiv);
            var chartData = this.getChartData(flotChartDiv);

            seriesToggleContainer.find("input[type='checkbox']").each(function() {
                var checkBox = $(this);
                if (checkBox.is(":checked")) {
                    var seriesIndex = parseInt(checkBox.attr("value"));
                    arrayOfSeriesToDraw.push(chartData["series"][seriesIndex]);
                }
            });

            return arrayOfSeriesToDraw;
        },

        setPlot : function(flotChartDiv, thePlot) {
            this.plots[this.getFlotChartDivIndex(flotChartDiv)] = thePlot;
        },

        getPlot : function(flotChartDiv) {
            var index = this.getFlotChartDivIndex(flotChartDiv);
            return index < this.plots.length ? this.plots[index] : null;
        },

        initChartPlaceholder : function(flotChartDiv) {
            var chartParams = this.getParams(this.getFieldset(flotChartDiv));
            var drawRegion = this.getDrawRegion(flotChartDiv);

            drawRegion.css({ width: chartParams["width"], height: chartParams["height"] });
            if (chartParams["leftMargin"])
                drawRegion.css({ "margin-left" : chartParams["leftMargin"] });

            drawRegion.bind("plotzoom", function(event, plot) {
                flotchart.getResetButtonContainer(flotChartDiv).show();
            });

            drawRegion.bind("plotpan", function(event, plot) {
                flotchart.getResetButtonContainer(flotChartDiv).show();
            });

            this.plotChart(flotChartDiv);
        },

        plotChart : function(flotChartDiv) {
            var thePlot = this.getPlot(flotChartDiv);
            if (thePlot) {
                thePlot = $.plot(this.getDrawRegion(flotChartDiv), this.getEnabledSeries(flotChartDiv), thePlot.getOptions());
            }
            else
            {
                var chartData = this.getChartData(flotChartDiv);
                if (chartData) {
                    thePlot = $.plot(this.getDrawRegion(flotChartDiv), this.getEnabledSeries(flotChartDiv), chartData["options"]);
                }
            }

            this.setPlot(flotChartDiv, thePlot);
        },

        getToolTipDiv : function(flotChartDiv) {
            var toolTipDiv = $("div.plugin_flotchart_toolip", flotChartDiv);
            if (!toolTipDiv.length) {
                toolTipDiv = $(document.createElement("div"));
                toolTipDiv.addClass("plugin_flotchart_toolip");
                toolTipDiv.appendTo(flotChartDiv);
            }

            return toolTipDiv;
        },

        initDataPointValueHover : function(flotChartDiv) {
            var chartData = this.getChartData(flotChartDiv);

            if (chartData.options && chartData.options.grid && chartData.options.grid.hoverable) {
                this.getDrawRegion(flotChartDiv).bind("plothover", function(event, pos, item) {
                    var toolTipDiv = flotchart.getToolTipDiv(flotChartDiv);

                    if (item && item.datapoint.length > 1) {
                        // Only show value when there's a Y value.
                        toolTipDiv.text(item.datapoint[1].toFixed(2));
                        toolTipDiv.css({
                            top : item.pageY + 5,
                            left: item.pageX + 5
                        }).fadeIn(200);
                    } else {
                        toolTipDiv.remove();
                    }
                });
            }
        },

        initDataPointNavigation : function(flotChartDiv) {
            this.getDrawRegion(flotChartDiv).bind("plotclick", function(event, pos, item) {
                if (item && item.series.dataPointLinks) {
                    var linkKey = item.datapoint[0]; // Link key may be zero
                    var link = null != linkKey ? item.series.dataPointLinks[linkKey.toString()] : null;
                    if (link) {
                        if (top)
                            top.location = link;
                        else
                            window.location = link;
                    }
                }
            });
        },

        initFlotChart : function(flotChartDiv) {
            this.initResetZoomButton(flotChartDiv);
            this.initDataSeries(flotChartDiv);
            this.initDataPointValueHover(flotChartDiv);
            this.initDataPointNavigation(flotChartDiv);
            this.initChartPlaceholder(flotChartDiv);
        }
    };

    $("div.plugin_flotchart").each(function(flotChartDivIndex) {
        $.data(this, "elementIndex", flotChartDivIndex);
        flotchart.initFlotChart($(this));
    });
});