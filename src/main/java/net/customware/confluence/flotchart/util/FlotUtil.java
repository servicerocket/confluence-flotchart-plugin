package net.customware.confluence.flotchart.util;

import static com.atlassian.core.util.DateUtils.DAY_MILLIS;
import static com.atlassian.core.util.DateUtils.MINUTE_MILLIS;
import static com.atlassian.core.util.DateUtils.YEAR_MILLIS;
import org.apache.commons.lang.ArrayUtils;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class is mainly responsible for translating data (like date formats) in Flot format to Java equivalents.
 * If there's a difference in how Flot and Java handle certain data, this should be where you can find all the conversions.
 */
public class FlotUtil
{
    /**
     * The valid Flot date format characters.
     */
    public static final char[] FLOT_DATE_CHARS = new char[] { 'S', 'H', 'h', 'M', 'd', 'm', 'b', 'y' };

    /**
     * The base series colors used by Flot.
     */
    private static List<Color> BASE_SERIES_COLORS = Arrays.asList(
            new Color(0xed, 0xc2, 0x40),
            new Color(0xaf, 0xd8, 0xf8),
            new Color(0xcb, 0x4b, 0x4b),
            new Color(0x4d, 0xa7, 0x4d),
            new Color(0x94, 0x40, 0xed)
    );

    /**
     * Gets the Flot date format based on the specified average time tick size.
     * @param averageTimeSeriesTickSizeInMillis
     * The tick size, in milliseconds.
     * @return
     * A suitable Flot date format.
     */
    public static String calculateSuitableFlotTimeFormat(long averageTimeSeriesTickSizeInMillis)
    {
        if (averageTimeSeriesTickSizeInMillis < MINUTE_MILLIS)
            return "%h:%M:%S";
        if (averageTimeSeriesTickSizeInMillis < DAY_MILLIS)
            return "%h:%M";
        if (averageTimeSeriesTickSizeInMillis < DAY_MILLIS * 28)
            return "%b %d";
        if (averageTimeSeriesTickSizeInMillis < YEAR_MILLIS)
            return "%b";
        else
            return "%y";
    }

    /**
     * Converts the specified Flot date format to Java date format.
     * @param flotFormat
     * The Flot date format.
     * @return
     * A date format that {@link java.text.SimpleDateFormat} understands.
     * @see
     * {@link #calculateSuitableFlotTimeFormat(long)} 
     */
    public static String convertFlotTimeSeriesFormatToJavaDateFormat(String flotFormat)
    {
        int flotFormatLength = flotFormat.length();
        StringBuilder javaDateFormatBuilder = new StringBuilder();
        boolean escape = false;
        boolean inLiteral = false;

        for (int i = 0; i < flotFormatLength; ++i)
        {
            char c = flotFormat.charAt(i);
            if (escape)
            {
                if (ArrayUtils.contains(FLOT_DATE_CHARS, c))
                {
                    if (inLiteral)
                        javaDateFormatBuilder.append('\'');
                    
                    switch (c)
                    {
                        case 'S':
                            javaDateFormatBuilder.append("ss");
                            break;
                        case 'h':
                            javaDateFormatBuilder.append('H');
                            break;
                        case 'H':
                            javaDateFormatBuilder.append("HH");
                            break;
                        case 'M':
                            javaDateFormatBuilder.append("mm");
                            break;
                        case 'd':
                            javaDateFormatBuilder.append('d');
                            break;
                        case 'm':
                            javaDateFormatBuilder.append('M');
                            break;
                        case 'b':
                            javaDateFormatBuilder.append("MMM");
                            break;
                        case 'y':
                            javaDateFormatBuilder.append("yyyy");
                            break;
                    }
                }
                else
                {
                    inLiteral = true;
                    javaDateFormatBuilder.append("'%").append(c);
                }

                escape = false;
            }
            else
            {
                if (c == '%')
                {
                    escape = true;
                    if (inLiteral)
                    {
                        javaDateFormatBuilder.append('\'');
                        inLiteral = false;
                    }
                }
                else
                {
                    if (!inLiteral)
                    {
                        javaDateFormatBuilder.append('\'');
                        inLiteral = true;
                    }

                    javaDateFormatBuilder.append(c);
                }
            }
        }

        if (inLiteral)
            javaDateFormatBuilder.append('\'');


        return javaDateFormatBuilder.toString();
    }

    private static int normalizeColorValue(int min, int max, int actual)
    {
        if (actual < min)
            return min;
        if (actual > max)
            return max;
        return actual;
    }

    /**
     * Based on the color generation algorithm fillInSeriesOptions() in flot.js. This is used for series color
     * consistency between Flot and JFreeChart.
     * 
     * @param colorsWanted
     * The number of colors wanted.
     * @param existingColors
     * The existing or user-specified colors which will be at the top of the list returned.
     * @return
     * A {@link java.util.List} of {@link String} representing the colors in hex (e.g. <tt>#ffaabb</tt>).
     * The top of the list will contain all colors specified in <tt>existingColors</tt>.
     */
    public static List<String> generateColorsForSeries(int colorsWanted, List<String> existingColors)
    {
        int colorsStillNeeded = colorsWanted - existingColors.size();

        List<String> filledColors = new ArrayList<String>();
        int variation = 0;
        int i = 0;
        StringBuilder colorBuilder = new StringBuilder();

        while(filledColors.size() < colorsStillNeeded)
        {
            int r, g, b;
            if (i == BASE_SERIES_COLORS.size())
            {
                r = 0x64;
                g = 0x64;
                b = 0x64;
            }
            else
            {
                Color color = BASE_SERIES_COLORS.get(i);
                r = color.getRed();
                g = color.getGreen();
                b = color.getBlue();
            }

            int sign = variation % 2 == 1 ? -1 : 1;
            double scale = 1 + sign * Math.ceil(variation / 2) * 0.2;

            r = normalizeColorValue(0, 255, (int) (scale * r));
            g = normalizeColorValue(0, 255, (int) (scale * g));
            b = normalizeColorValue(0, 255, (int) (scale * b));

            colorBuilder.setLength(0);
            colorBuilder.append('#')
                    .append(Integer.toHexString(r))
                    .append(Integer.toHexString(g))
                    .append(Integer.toHexString(b));

            filledColors.add(colorBuilder.toString());

            if (++i >= BASE_SERIES_COLORS.size())
            {
                i = 0;
                ++variation;
            }
        }

        List<String> finalColors = new ArrayList<String>(existingColors);
        finalColors.addAll(filledColors);

        return finalColors;
    }
}
