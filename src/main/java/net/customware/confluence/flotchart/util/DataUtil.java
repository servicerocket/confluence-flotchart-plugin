package net.customware.confluence.flotchart.util;

import net.customware.confluence.flotchart.model.Chart;
import net.customware.confluence.flotchart.model.options.AxisOptions;
import net.customware.confluence.flotchart.model.options.NumberAxisOptions;
import net.customware.confluence.flotchart.model.series.DataPoint;
import net.customware.confluence.flotchart.model.series.Series;
import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class helps to perform data manipulation and conversion to {@link Chart}.
 */
public class DataUtil
{
    /**
     * The valid data models, whether the grid data given to this class is vertically or horizontally oriented.
     */
    public static enum DataModel
    {
        VERTICAL, HORIZONTAL
    }

    /**
     * The chart's x-axis.
     */
    private AxisOptions xAxis;

    /**
     * The chart's y-axis.
     */
    private NumberAxisOptions yAxis;

    /**
     * The data model to assume in data manipulation methods.
     */
    private DataModel dataModel;

    /**
     * Indicates whether the {@link net.customware.confluence.flotchart.model.series.Series} in the
     * {@link net.customware.confluence.flotchart.model.Chart} generated by the {@link #toChart(java.util.List)}
     * method should contain only one value.
     *
     * This is useful for charts that can only contain one value per series (e.g. pie chart).
     */
    private boolean oneValuePerSeries;

    /**
     * Constructs a new instance of {@link net.customware.confluence.flotchart.util.DataUtil}
     * @param xAxis
     * The options for the x-axis
     * @param numberAxisOptions
     * The options for the y-axis
     * @param dataModel
     * The data orientation to assume when processing data.
     * @param oneValuePerSeries
     * Defines whether series in a chart should contain only one value. See also {@link net.customware.confluence.flotchart.model.series.Series#oneValuePerSeries}.
     */
    public DataUtil(AxisOptions xAxis, NumberAxisOptions numberAxisOptions, DataModel dataModel, boolean oneValuePerSeries)
    {
        this.xAxis = xAxis;
        this.yAxis = numberAxisOptions;
        this.dataModel = dataModel;
        this.oneValuePerSeries = oneValuePerSeries;
    }

    /**
     * Rotates the specified data ninety degress clock wise.
     * @param source
     * The data to rotate
     * @return
     * A copy of the specified data, rotated ninety degress.
     */
    public String[][] rotateNinetyDegreesClockWise(String[][] source)
    {
        if (source.length == 0)
            throw new IllegalArgumentException("Source grid does not have any rows.");

        int columnCount = source[0].length;

        String[][] newGrid = new String[columnCount][source.length];

        for (int i = 0; i < columnCount; ++i)
        {
            newGrid[i] = new String[source.length];

            for (int j = 0; j < source.length; ++j)
            {
                newGrid[i][j] = source[source.length - 1 - j][i];
            }
        }

        return newGrid;
    }

    private int getIndexOfStringInSet(Set<String> stringSet, String toSearch)
    {
        int index = 0;
        for (String string : stringSet)
        {
            if (StringUtils.equals(string, toSearch))
                return index;
            ++index;
        }

        return -1;
    }

    /**
     * Converts a grid of {@link String} to a {@link net.customware.confluence.flotchart.model.Chart}.
     * @param grids
     * The data to process. Its orientation will be what is provided to this class during its construction.
     * @return
     * An instance of {@link net.customware.confluence.flotchart.model.Chart} with the axis options provided to this
     * class during its construction
     * @throws ParseException
     * Thrown when there is a problem converting the data to a {@link net.customware.confluence.flotchart.model.Chart}.
     */
    public Chart toChart(List<CellValue[][]> grids) throws ParseException
    {
        Chart chart = new Chart(xAxis, yAxis);
        Map<String, Series> seriesMap = new LinkedHashMap<String, Series>();
        Set<String> domainValueLabels = new LinkedHashSet<String>();
        boolean isXAxisScalable = xAxis.isScalable();
        AxisOptions.TickSize minTickSize = xAxis.getMinTickSize();
        
        for (CellValue[][] grid : grids)
        {
            if (DataModel.VERTICAL.equals(dataModel))
            {
                for (int i = 1; i < grid.length; ++i)
                {
                    for (int j = 1; j < grid[i].length; ++j)
                    {
                        String seriesLabel = grid[0][j].getText();

                        if (StringUtils.isBlank(seriesLabel))
                            continue; // No point processing if there is no series label (unidentified series)

                        Series aSeries = seriesMap.containsKey(seriesLabel)
                                ? seriesMap.get(seriesLabel)
                                : new Series(seriesLabel, oneValuePerSeries);

                        seriesMap.put(seriesLabel, aSeries);
                        domainValueLabels.add(grid[i][0].getText());

                        Comparable x = isXAxisScalable
                                ? xAxis.toAxisValue(grid[i][0].getText())
                                : (null != minTickSize ? getIndexOfStringInSet(domainValueLabels, grid[i][0].getText()) * xAxis.getMinTickSize().getSize() : null);
                        Comparable y = yAxis.toAxisValue(grid[i][j].getText());

                        if (null != x)
                        {
                            aSeries.addData(new DataPoint(x, y));

                            if (StringUtils.isNotBlank(grid[i][j].getLink()))
                                aSeries.addDataPointLink(x, grid[i][j].getLink());

                            if (!isXAxisScalable && !oneValuePerSeries)
                                xAxis.addTick(x, grid[i][0].getText());
                        }
                    }
                }
            }
            else
            {

                for (int i = 1; i < grid.length; ++i)
                {
                    String seriesLabel = grid[i][0].getText();
                    if (StringUtils.isBlank(seriesLabel))
                        continue; // No point processing if there is no series label (unidentified series)

                    Series aSeries = seriesMap.containsKey(seriesLabel)
                            ? seriesMap.get(seriesLabel)
                            : new Series(seriesLabel, oneValuePerSeries);

                    seriesMap.put(seriesLabel, aSeries);

                    for (int j = 1; j < grid[i].length; ++j)
                    {
                        domainValueLabels.add(grid[0][j].getText());

                        Comparable x = isXAxisScalable
                                ? xAxis.toAxisValue(grid[0][j].getText())
                                : (null != minTickSize ? getIndexOfStringInSet(domainValueLabels, grid[0][j].getText()) * xAxis.getMinTickSize().getSize() : null);
                        Comparable y = yAxis.toAxisValue(grid[i][j].getText());

                        if (null != x)
                        {
                            aSeries.addData(new DataPoint(x, y));
                            
                            if (StringUtils.isNotBlank(grid[i][j].getLink()))
                                aSeries.addDataPointLink(x, grid[i][j].getLink());

                            if (!isXAxisScalable && !oneValuePerSeries)
                                xAxis.addTick(x, grid[0][j].getText());
                        }
                    }
                }
            }
        }

        for (Series series : seriesMap.values())
            chart.addSeries(series);

        return chart;
    }
}
