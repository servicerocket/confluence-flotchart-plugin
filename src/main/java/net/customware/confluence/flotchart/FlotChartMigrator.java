package net.customware.confluence.flotchart;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.definition.PlainTextMacroBody;
import com.atlassian.confluence.content.render.xhtml.definition.RichTextMacroBody;
import com.atlassian.confluence.macro.xhtml.MacroMigration;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class FlotChartMigrator implements MacroMigration
{
    private static final Logger LOG = LoggerFactory.getLogger(FlotChartMigrator.class);

    private final XhtmlContent xhtmlContent;

    public FlotChartMigrator(XhtmlContent xhtmlContent)
    {
        this.xhtmlContent = xhtmlContent;
    }

    public MacroDefinition migrate(MacroDefinition macroDefinition, ConversionContext conversionContext)
    {
        String macroBody = StringUtils.defaultString(macroDefinition.getBodyText());
        List<RuntimeException> conversionErrors = new ArrayList<RuntimeException>();

        if (StringUtils.isNotBlank(macroBody))
        {
            macroDefinition.setBody(
                    new RichTextMacroBody(
                            xhtmlContent.convertWikiToStorage(
                                    macroBody,
                                    conversionContext,
                                    conversionErrors
                            )
                    )
            );
        }

        if (!conversionErrors.isEmpty())
            for (RuntimeException conversionError : conversionErrors)
                LOG.error(String.format("Unable to convert wiki markup below to XHTML storage:\n%s", macroBody), conversionError);

        return macroDefinition;
    }
}
