package net.customware.confluence.flotchart.model.options;

import net.customware.confluence.flotchart.model.JsonModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.TreeMap;

public abstract class AxisOptions<T extends AxisOptions.TickSize> implements JsonModel
{
    private Number min;

    private Number max;

    private Map<Comparable, String> ticksMap;

    protected AxisOptions()
    {
        ticksMap = new TreeMap<Comparable, String>();
    }

    public Number getMin()
    {
        return min;
    }

    public void setMin(Number min)
    {
        this.min = min;
    }

    public Number getMax()
    {
        return max;
    }

    public void setMax(Number max)
    {
        this.max = max;
    }

    public abstract T getMinTickSize();

    public abstract void setMinTickSize(T minTickSize);

    public abstract T getTickSize();

    public abstract void setTickSize(T tickSize);

    public abstract String getMode();

    public abstract Comparable toAxisValue(String stringValue);

    public abstract boolean isScalable();

    public void addTick(Comparable x, String label)
    {
        ticksMap.put(x, label);
    }

    public String getTickLabel(Comparable x)
    {
        return ticksMap.get(x);
    }

    public JSONObject toJson() throws JSONException
    {
        JSONObject thisObject = new JSONObject();

        if (null != getMin())
            thisObject.put("min", getMin());

        if (null != getMax())
            thisObject.put("max", getMax());

        if (null != getMode())
            thisObject.put("mode", getMode());

        if (!ticksMap.isEmpty())
        {
            JSONArray ticksArray = new JSONArray();

            for (Map.Entry<Comparable, String> tickEntry : ticksMap.entrySet())
            {
                JSONArray tickJson = new JSONArray();

                tickJson.put(tickEntry.getKey());
                tickJson.put(tickEntry.getValue());

                ticksArray.put(tickJson);
            }

            thisObject.put("ticks", ticksArray);
        }

        return thisObject;
    }

    public static interface TickSize
    {
        int getSize();
    }
}
