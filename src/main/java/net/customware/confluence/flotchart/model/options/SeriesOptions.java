package net.customware.confluence.flotchart.model.options;

import net.customware.confluence.flotchart.model.series.LineCustomization;
import net.customware.confluence.flotchart.model.series.PointCustomization;
import net.customware.confluence.flotchart.model.series.BarCustomization;
import net.customware.confluence.flotchart.model.series.PieCustomization;
import net.customware.confluence.flotchart.model.JsonModel;

import java.util.Map;
import java.util.HashMap;

import org.json.JSONObject;
import org.json.JSONException;

public class SeriesOptions implements JsonModel
{
    private static enum Type {
        PIE,
        LINE,
        BAR,
        POINT
    }

    private Map<Type, LineCustomization> seriesCustomizationMap;

    public SeriesOptions()
    {
        this.seriesCustomizationMap = new HashMap<Type, LineCustomization>();
    }

    public boolean isSet()
    {
        return !seriesCustomizationMap.isEmpty();
    }

    private void _asLine(LineCustomization lineCustomization, PointCustomization pointCustomization)
    {
        seriesCustomizationMap.clear();
        seriesCustomizationMap.put(
                Type.LINE,
                lineCustomization
        );
        seriesCustomizationMap.put(
                Type.POINT,
                pointCustomization
        );
    }

    public void asLine(LineCustomization lineCustomization)
    {
        _asLine(lineCustomization, null);
    }

    public void asLine(LineCustomization lineCustomization, PointCustomization pointCustomization)
    {
        _asLine(lineCustomization, pointCustomization);
    }

    public void asPoint(PointCustomization pointCustomization)
    {
        seriesCustomizationMap.clear();
        seriesCustomizationMap.put(
                Type.POINT,
                pointCustomization
        );
    }

    public void asBar(BarCustomization barCustomization)
    {
        seriesCustomizationMap.clear();
        seriesCustomizationMap.put(
                Type.BAR,
                barCustomization
        );
    }

    public void asPie(PieCustomization pieCustomization)
    {
        seriesCustomizationMap.clear();
        seriesCustomizationMap.put(
                Type.PIE,
                pieCustomization
        );
    }

    public LineCustomization getLines()
    {
        return seriesCustomizationMap.get(Type.LINE);
    }

    public PointCustomization getPoints()
    {
        return (PointCustomization) seriesCustomizationMap.get(Type.POINT);
    }

    public BarCustomization getBars()
    {
        return (BarCustomization) seriesCustomizationMap.get(Type.BAR);
    }

    public PieCustomization getPie()
    {
        return (PieCustomization) seriesCustomizationMap.get(Type.PIE);
    }

    public JSONObject toJson() throws JSONException
    {
        JSONObject thisObject = new JSONObject();

        if (null != getLines())
            thisObject.put("lines", getLines().toJson());

        if (null != getPoints())
            thisObject.put("points", getPoints().toJson());

        if (null != getBars())
        {
            thisObject.put("bars", getBars().toJson());
            thisObject.put("stack", true); // Activate stacked bar chart
        }

        if (null != getPie())
            thisObject.put("pie", getPie().toJson());

        return thisObject;
    }
}
