package net.customware.confluence.flotchart.model.series;

import org.json.JSONObject;
import org.json.JSONException;


public class PointCustomization extends LineCustomization
{
    private Number radius;

    public Number getRadius()
    {
        return radius;
    }

    public void setRadius(Number radius)
    {
        this.radius = radius;
    }

    @Override
    public JSONObject toJson() throws JSONException
    {
        JSONObject superJson = super.toJson();

        if (null != getRadius())
            superJson.put("radius", getRadius());

        return superJson;
    }
}
