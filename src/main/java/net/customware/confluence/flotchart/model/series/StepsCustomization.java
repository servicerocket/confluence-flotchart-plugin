package net.customware.confluence.flotchart.model.series;

import org.json.JSONObject;
import org.json.JSONException;


public class StepsCustomization extends LineCustomization
{
    private boolean steps;

    public StepsCustomization()
    {
        this.steps = true;
    }

    public boolean isSteps()
    {
        return steps;
    }

    public void setSteps(boolean steps)
    {
        this.steps = steps;
    }

    @Override
    public JSONObject toJson() throws JSONException
    {
        JSONObject thisObject = super.toJson();
        thisObject.put("steps", isSteps());
        return thisObject;
    }
}
