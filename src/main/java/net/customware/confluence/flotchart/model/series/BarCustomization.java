package net.customware.confluence.flotchart.model.series;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.json.JSONException;

public class BarCustomization extends LineCustomization
{
    public static enum Alignment {
        LEFT, CENTER 
    }

    private Number barWidth;

    private String align;

    private boolean horizontal;

    public BarCustomization()
    {
        setAlign(Alignment.CENTER.toString());
    }

    public Number getBarWidth()
    {
        return barWidth;
    }

    public void setBarWidth(Number barWidth)
    {
        this.barWidth = barWidth;
    }

    public String getAlign()
    {
        return StringUtils.lowerCase(align);
    }

    public void setAlign(String align)
    {
        this.align = Alignment.valueOf(StringUtils.upperCase(align)).toString();
    }

    public boolean isHorizontal()
    {
        return horizontal;
    }

    public void setHorizontal(boolean horizontal)
    {
        this.horizontal = horizontal;
    }

    @Override
    public JSONObject toJson() throws JSONException
    {
        JSONObject superJson = super.toJson();

        if (null != getBarWidth())
            superJson.put("barWidth", getBarWidth());

        if (null != getAlign())
            superJson.put("align", getAlign());

        superJson.put("horizontal", isHorizontal());

        return superJson;
    }
}
