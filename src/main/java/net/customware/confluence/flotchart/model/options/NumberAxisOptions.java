package net.customware.confluence.flotchart.model.options;

import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class NumberAxisOptions extends AxisOptions<NumberTickSize>
{
    private static final Logger logger = LoggerFactory.getLogger(TimeSeriesAxisOptions.class);

    private List<NumberFormat> builtInDecimalFormats;

    private NumberFormat numberFormat;

    private NumberTickSize minTickSize;

    private NumberTickSize tickSize;

    public NumberAxisOptions(String numberFormat, Locale locale)
    {
        setNumberFormat(numberFormat);
        builtInDecimalFormats = Arrays.asList(
                NumberFormat.getNumberInstance(locale),
                NumberFormat.getPercentInstance(locale),
                NumberFormat.getCurrencyInstance(locale)
        );
    }

    public NumberAxisOptions(Locale locale)
    {
        this(null, locale);
    }

    public String getMode()
    {
        return null;
    }

    public void setNumberFormat(String numberFormat)
    {
        this.numberFormat = StringUtils.isNotBlank(numberFormat) ? new DecimalFormat(numberFormat) : null;
    }

    private Comparable tryConvertStringValueWithBulitInNumberFormats(String numberValueString)
    {
        for (NumberFormat numberFormat : builtInDecimalFormats)
        {
            try
            {
                Number number = numberFormat.parse(numberValueString);
                return number.doubleValue();
            }
            catch (ParseException pe)
            {
                logger.debug("Can't convert " + numberValueString + " to a number with the format " + numberFormat);
            }
        }

        return null;
    }

    public Comparable toAxisValue(String stringValue)
    {
        String numberValueString = StringUtils.trim(stringValue);

        try
        {
            if (StringUtils.isNotBlank(numberValueString))
            {
                if (null != numberFormat)
                {
                    Number value = numberFormat.parse(numberValueString);
                    return null != value ? value.doubleValue() : null;
                }
                else
                {
                    if (StringUtils.isNumeric(numberValueString))
                        return new Long(numberValueString);
                    else
                        return new Double(numberValueString);
                }
            }
        }
        catch (NumberFormatException nfe)
        {
            logger.debug("Unable to convert " + stringValue + " to a number", nfe);
            return tryConvertStringValueWithBulitInNumberFormats(numberValueString);
        }
        catch (ParseException pe)
        {
            logger.debug("Unable to parse " + numberValueString + " as a number", pe);
        }

        return null;
    }

    public boolean isScalable()
    {
        return true;
    }

    public NumberTickSize getMinTickSize()
    {
        return minTickSize;
    }

    public void setMinTickSize(NumberTickSize minTickSize)
    {
        this.minTickSize = minTickSize;
    }

    public NumberTickSize getTickSize()
    {
        return tickSize;
    }

    public void setTickSize(NumberTickSize tickSize)
    {
        this.tickSize = tickSize;
    }

    @Override
    public JSONObject toJson() throws JSONException
    {
        JSONObject thisObject = super.toJson();

        NumberTickSize minTickSize = getMinTickSize();
        if (null != minTickSize)
            thisObject.put("minTickSize", minTickSize.getSize());

        NumberTickSize tickSize = getTickSize();
        if (null != tickSize)
            thisObject.put("tickSize", tickSize.getSize());
            

        return thisObject;
    }

}
