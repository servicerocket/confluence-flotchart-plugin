package net.customware.confluence.flotchart.model;

import net.customware.confluence.flotchart.model.options.PlotOptions;
import net.customware.confluence.flotchart.model.options.AxisOptions;
import net.customware.confluence.flotchart.model.series.Series;

import java.util.List;
import java.util.ArrayList;

import org.json.JSONObject;
import org.json.JSONException;
import org.json.JSONArray;

/**
 * The class that represents a Flot chart object
 */
public class Chart implements JsonModel
{
    /**
     * The data series.
     */
    private List<Series> series;

    /**
     * How the chart should be plotted/rendered.
     */
    private PlotOptions options;

    /**
     * Creates a new {@link net.customware.confluence.flotchart.model.Chart}.
     * @param xAxisOptions
     * The x-axis options.
     * @param yAxisOptions
     * The y-axis options.
     */
    public Chart(AxisOptions xAxisOptions, AxisOptions yAxisOptions)
    {
        series = new ArrayList<Series>();
        options = new PlotOptions(xAxisOptions, yAxisOptions);
    }

    public List<Series> getSeries()
    {
        return series;
    }

    public void addSeries(Series series)
    {
        this.series.add(series);
    }

    public PlotOptions getOptions()
    {
        return options;
    }

    public void setOptions(PlotOptions options)
    {
        this.options = options;
    }

    public JSONObject toJson() throws JSONException
    {
        JSONObject thisObject = new JSONObject();

        if (null != getSeries())
        {
            JSONArray seriesArrayJson = new JSONArray();

            for (Series series : getSeries())
                seriesArrayJson.put(series.toJson());

            thisObject.put("series", seriesArrayJson);
        }

        if (null != getOptions())
            thisObject.put("options", getOptions().toJson());

        return thisObject;
    }
}
