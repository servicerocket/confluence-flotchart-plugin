package net.customware.confluence.flotchart.model.options;

import net.customware.confluence.flotchart.model.JsonModel;
import net.customware.confluence.flotchart.model.series.BarCustomization;
import net.customware.confluence.flotchart.model.series.LineCustomization;
import net.customware.confluence.flotchart.model.series.PieCustomization;
import net.customware.confluence.flotchart.model.series.PointCustomization;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class defines the plot options for a {@link net.customware.confluence.flotchart.model.Chart}.
 */
public class PlotOptions implements JsonModel
{
    private LegendOptions legendOptions;

    private AxisOptions xaxis;

    private AxisOptions yaxis;

    private SeriesOptions seriesOptions;

    private GridOptions gridOptions;

    public PlotOptions(AxisOptions xaxis, AxisOptions yaxis)
    {
        this.xaxis = xaxis;
        this.yaxis = yaxis;
        this.seriesOptions = new SeriesOptions();
        this.gridOptions = new GridOptions();
    }

    public LegendOptions getLegendOptions()
    {
        return legendOptions;
    }

    public void setLegendOptions(LegendOptions legendOptions)
    {
        this.legendOptions = legendOptions;
    }

    public AxisOptions getXaxis()
    {
        return xaxis;
    }

    public void setXaxis(AxisOptions xaxis)
    {
        this.xaxis = xaxis;
    }

    public AxisOptions getYaxis()
    {
        return yaxis;
    }

    public void setYaxis(AxisOptions yaxis)
    {
        this.yaxis = yaxis;
    }

    public SeriesOptions getSeries()
    {
        return seriesOptions;
    }

    public void asLine(LineCustomization lineCustomization)
    {
        seriesOptions.asLine(lineCustomization);
    }

    public void asLine(LineCustomization lineCustomization, PointCustomization pointCustomization)
    {
        seriesOptions.asLine(lineCustomization, pointCustomization);
    }

    public void asPoint(PointCustomization pointCustomization)
    {
        seriesOptions.asPoint(pointCustomization);
    }

    public void asBar(BarCustomization barCustomization)
    {
        seriesOptions.asBar(barCustomization);
    }

    public void asPie(PieCustomization pieCustomization)
    {
        seriesOptions.asPie(pieCustomization);
    }

    public GridOptions getGridOptions()
    {
        return gridOptions;
    }

    public void setGridOptions(GridOptions gridOptions)
    {
        this.gridOptions = gridOptions;
    }

    public JSONObject toJson() throws JSONException
    {
        JSONObject thisObject = new JSONObject();
        
        if (null != getLegendOptions())
            thisObject.put("legend", getLegendOptions().toJson());

        if (null != getXaxis())
            thisObject.put("xaxis", getXaxis().toJson());

        if (null != getYaxis())
            thisObject.put("yaxis", getYaxis().toJson());

        if (seriesOptions.isSet())
            thisObject.put("series", seriesOptions.toJson());

        if (null != gridOptions)
            thisObject.put("grid", gridOptions.toJson());

        return thisObject;
    }
}
