package net.customware.confluence.flotchart.model.series;

import net.customware.confluence.flotchart.model.JsonModel;
import org.json.JSONObject;
import org.json.JSONException;


public class PieCustomization extends LineCustomization
{
    private Combine combine;

    private Number radius;

    private Label label;

    public void setCombineThreshold(Number number)
    {
        combine = new Combine(combine);
        combine.setThreshold(number);
    }

    public void setCombineLabel(String label)
    {
        combine = new Combine(combine);
        combine.setLabel(label);
    }

    public Combine getCombine()
    {
        return combine;
    }

    public Number getRadius()
    {
        return radius;
    }

    public void setRadius(Number radius)
    {
        this.radius = radius;
    }

    public void setShowLabel(boolean show)
    {
        label = new Label(label);
        label.setShow(show);
    }

    public Label getLabel()
    {
        return label;
    }

    @Override
    public JSONObject toJson() throws JSONException
    {
        JSONObject thisObject = super.toJson();
        if (null != combine)
            thisObject.put("combine", combine.toJson());


        if (null != getRadius())
            thisObject.put("radius", getRadius());

        if (null != getLabel())
            thisObject.put("label", getLabel().toJson());

        return thisObject;
    }

    public static class Combine implements JsonModel
    {
        private Number threshold;

        private String label;

        public Combine(Combine toCopy)
        {
            if (null != toCopy)
            {
                setThreshold(toCopy.getThreshold());
                setLabel(toCopy.getLabel());
            }
        }

        public Number getThreshold()
        {
            return threshold;
        }

        public void setThreshold(Number threshold)
        {
            this.threshold = threshold;
        }

        public String getLabel()
        {
            return label;
        }

        public void setLabel(String label)
        {
            this.label = label;
        }

        public JSONObject toJson() throws JSONException
        {
            JSONObject thisObject = new JSONObject();

            if (null != getThreshold())
                thisObject.put("threshold", getThreshold());
            if (null != getLabel())
                thisObject.put("label", getLabel());

            return thisObject;
        }
    }

    public static class Label implements JsonModel
    {
        private boolean show;

        public Label(Label toCopy)
        {
            setShow(null == toCopy || toCopy.isShow());
        }

        public boolean isShow()
        {
            return show;
        }

        public void setShow(boolean show)
        {
            this.show = show;
        }

        public JSONObject toJson() throws JSONException
        {
            JSONObject thisObject = new JSONObject();
            thisObject.put("show", show);
            return thisObject;
        }
    }
}
