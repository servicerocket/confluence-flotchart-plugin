package net.customware.confluence.flotchart.model.options;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TimeSeriesAxisOptions extends AxisOptions<TimeSeriesTickSize>
{
    private static final Logger logger = LoggerFactory.getLogger(TimeSeriesAxisOptions.class);

    public static enum TickSizeInterval
    {
        SECOND, MINUTE, HOUR, DAY, MONTH, YEAR
    }

    private Locale locale;

    private String monthNameFormat;

    private String timeFormat;

    private DateFormat dateFormat;

    private TimeSeriesTickSize minTickSize;

    private TimeSeriesTickSize tickSize;

    public TimeSeriesAxisOptions(Locale locale, String monthNameFormat, String valueFormat, String flotDisplayFormat)
    {
        this.locale = null == locale ? Locale.getDefault() : locale;
        this.monthNameFormat = StringUtils.defaultString(monthNameFormat, "MMM");
        dateFormat = new SimpleDateFormat(valueFormat);
        timeFormat = flotDisplayFormat;
    }

    public String getMode()
    {
        return "time";
    }

    public Long toAxisValue(String stringValue)
    {
        if (StringUtils.isNotBlank(stringValue))
            try
            {
                return dateFormat.parse(StringUtils.trim(stringValue)).getTime();
            }
            catch (ParseException pe)
            {
                logger.debug("Unable to parse " + stringValue + " as date with format " + timeFormat, pe);
            }
        return null;
    }

    public boolean isScalable()
    {
        return true;
    }

    public TimeSeriesTickSize getMinTickSize()
    {
        return minTickSize;
    }

    public void setMinTickSize(TimeSeriesTickSize minTickSize)
    {
        this.minTickSize = minTickSize;
    }

    public TimeSeriesTickSize getTickSize()
    {
        return tickSize;
    }

    public void setTickSize(TimeSeriesTickSize tickSize)
    {
        this.tickSize = tickSize;
    }

    public String[] getMonthNames()
    {
        try
        {
            Date epoc = new SimpleDateFormat("d/M/yyyy").parse("1/1/1970");
            Calendar epocCalendar = Calendar.getInstance();
            epocCalendar.setTime(epoc);

            String[] monthNamesArray = new String[epocCalendar.getMaximum(Calendar.MONTH) + 1];
            DateFormat monthNameFormatter = new SimpleDateFormat(monthNameFormat, locale);

            for (int i = 0; i < monthNamesArray.length; ++i)
            {
                monthNamesArray[i] = monthNameFormatter.format(epocCalendar.getTime());
                epocCalendar.add(Calendar.MONTH, 1);
            }

            return monthNamesArray;
        }
        catch (ParseException pe)
        {
            logger.debug("What the fuck? I can't parse hardcoded dates?", pe);
        }

        return null;
    }

    public String getTimeFormat()
    {
        return timeFormat;
    }

    public void setTimeFormat(String timeFormat)
    {
        this.timeFormat = timeFormat;
        dateFormat = new SimpleDateFormat(timeFormat);
    }

    @Override
    public JSONObject toJson() throws JSONException
    {
        JSONObject thisObject = super.toJson();

        TimeSeriesTickSize minTickSize = getMinTickSize();
        if (null != minTickSize)
        {
            JSONArray ticksArray = new JSONArray();
            ticksArray.put(minTickSize.getSize());
            ticksArray.put(StringUtils.lowerCase(minTickSize.getInterval().toString()));
            thisObject.put("minTickSize", ticksArray);
        }

        TimeSeriesTickSize tickSize = getTickSize();
        if (null != tickSize)
        {
            JSONArray ticksArray = new JSONArray();
            ticksArray.put(tickSize.getSize());
            ticksArray.put(StringUtils.lowerCase(tickSize.getInterval().toString()));
            thisObject.put("tickSize", ticksArray);
        }

        if (null != getMonthNames())
        {
            JSONArray monthNamesArrayJson = new JSONArray();

            for (String monthName : getMonthNames())
                monthNamesArrayJson.put(monthName);

            thisObject.put("monthNames", monthNamesArrayJson);
        }

        if (null != getTimeFormat())
            thisObject.put("timeformat", getTimeFormat());

        return thisObject;
    }

}
