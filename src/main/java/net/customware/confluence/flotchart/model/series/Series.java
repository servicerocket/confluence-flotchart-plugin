package net.customware.confluence.flotchart.model.series;

import net.customware.confluence.flotchart.model.JsonModel;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

/**
 * The class that represents a single data series in a chart.
 */
public class Series implements JsonModel
{
    /** The color of the series. */
    private String color;

    /** The name of the series. */
    private String label;

    /** Indicates whether this series contains only a value (only useful for pie charts for now). */
    private boolean oneValuePerSeries;

    /** The data points in the series. */
    private Collection<DataPoint> data;

    /** The collection of links in data points */
    private Map<Comparable, String> dataPointLinks;

    /**
     * Constructs a new data series.
     * @param label
     * The name of the series.
     * @param data
     * The data points in the series
     * @param oneValuePerSeries
     * Whether this series contains only a single value. This should be <tt>true</tt> if this series
     * represents a pie chart segment.
     */
    public Series(String label, Collection<DataPoint> data, boolean oneValuePerSeries)
    {
        this.label = label;
        this.oneValuePerSeries = oneValuePerSeries;
        this.data = this.oneValuePerSeries ? new ArrayList<DataPoint>() : new TreeSet<DataPoint>();
        this.data.addAll(data);
        this.dataPointLinks = new HashMap<Comparable, String>();
    }

    /**
     * Calls {@link #Series(String, java.util.Collection, boolean)} with an empty {@link net.customware.confluence.flotchart.model.series.DataPoint} list.
     * @param label
     * The series name.
     * @param oneValuePerSeries
     * Whether this series contains only a single value. This should be <tt>true</tt> if this series
     * represents a pie chart segment.
     */
    public Series(String label, boolean oneValuePerSeries)
    {
        this(label, Collections.<DataPoint> emptyList(), oneValuePerSeries);
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public Collection<DataPoint> getData()
    {
        return data;
    }

    public void addData(DataPoint dataPoint)
    {
        this.data.add(dataPoint);
    }

    public void addDataPointLink(Comparable xValue, String link)
    {
        dataPointLinks.put(xValue, link);
    }

    public JSONObject toJson() throws JSONException
    {
        JSONObject thisObject = new JSONObject();

        if (StringUtils.isNotBlank(getColor()))
            thisObject.put("color", getColor());

        if (StringUtils.isNotBlank(getLabel()))
            thisObject.put("label", getLabel());

        if (oneValuePerSeries)
        {
            if (!getData().isEmpty())
                for (DataPoint dataPoint : getData())
                    thisObject.put("data", dataPoint.getRangeValue());
        }
        else
        {
            JSONArray dataPointArray = new JSONArray();
            for (DataPoint dataPoint : getData())
            {
                JSONArray _dataPoint = new JSONArray();

                _dataPoint.put(dataPoint.getDomainValue());
                _dataPoint.put(dataPoint.getRangeValue());

                dataPointArray.put(_dataPoint);

                thisObject.put("data", dataPointArray);
            }

            if (!dataPointLinks.isEmpty())
            {
                JSONObject dataPointLinksJson = new JSONObject();

                for (Map.Entry<Comparable, String> entry : dataPointLinks.entrySet())
                    dataPointLinksJson.put(entry.getKey().toString(), entry.getValue());

                thisObject.put("dataPointLinks", dataPointLinksJson);
            }
        }

        return thisObject;
    }
}
