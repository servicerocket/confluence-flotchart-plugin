package net.customware.confluence.flotchart.model.options;

import net.customware.confluence.flotchart.model.JsonModel;
import org.json.JSONException;
import org.json.JSONObject;

public class GridOptions implements JsonModel
{
    private boolean hoverable;

    private boolean clickable;

    public GridOptions()
    {
        setClickable(true);
    }

    public boolean isHoverable()
    {
        return hoverable;
    }

    public void setHoverable(boolean hoverable)
    {
        this.hoverable = hoverable;
    }

    public boolean isClickable()
    {
        return clickable;
    }

    public void setClickable(boolean clickable)
    {
        this.clickable = clickable;
    }

    public JSONObject toJson() throws JSONException
    {
        JSONObject thisObject = new JSONObject();

        thisObject.put("hoverable", isHoverable());
        thisObject.put("clickable", isClickable());
        
        return thisObject;
    }
}
