package net.customware.confluence.flotchart.model;

import org.json.JSONObject;
import org.json.JSONException;

/**
 * The interface that defines the method that can be called on implementations to
 * get a {@link org.json.JSONObject} equivalent of it. It's really like the
 * {@link Object#toString()} method (to get a representation of an object), but for JSON specifically.
 */
public interface JsonModel
{
    /**
     * Implementations should return a {@link org.json.JSONObject} that describes it.
     * @return
     * A {@link org.json.JSONObject} that describes it.
     * @throws JSONException
     * Implementations may throw this exception to indicate any sort of fatal error in the conversion.
     */
    JSONObject toJson() throws JSONException;
}
