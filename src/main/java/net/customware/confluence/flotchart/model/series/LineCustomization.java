package net.customware.confluence.flotchart.model.series;

import org.json.JSONObject;
import org.json.JSONException;
import net.customware.confluence.flotchart.model.JsonModel;


public class LineCustomization implements JsonModel
{
    private boolean show;

    private Number lineWidth;

    private Number fill;

    private String fillColor;

    public LineCustomization()
    {
        this.show = true;
    }

    public boolean isShow()
    {
        return show;
    }

    public void setShow(boolean show)
    {
        this.show = show;
    }

    public Number getLineWidth()
    {
        return lineWidth;
    }

    public void setLineWidth(Number lineWidth)
    {
        this.lineWidth = lineWidth;
    }

    public Number getFill()
    {
        return fill;
    }

    public void setFill(Number fill)
    {
        this.fill = fill;
    }

    public String getFillColor()
    {
        return fillColor;
    }

    public void setFillColor(String fillColor)
    {
        this.fillColor = fillColor;
    }

    public JSONObject toJson() throws JSONException
    {
        JSONObject thisObject = new JSONObject();

        thisObject.put("show", isShow());

        if (null != getLineWidth())
            thisObject.put("lineWidth", getLineWidth());

        if (null != getFill())
            thisObject.put("fill", getFill());

        if (null != getFillColor())
            thisObject.put("fillColor", getFillColor());

        return thisObject;
    }
}
