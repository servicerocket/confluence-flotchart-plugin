package net.customware.confluence.flotchart.model.options;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.json.JSONException;
import net.customware.confluence.flotchart.model.JsonModel;

public class LegendOptions implements JsonModel
{
    public static enum Position
    {
        NE, NW, SE, SW
    }

    private boolean show;

    private String position;

    public LegendOptions()
    {
        show = true;
    }

    public boolean isShow()
    {
        return show;
    }

    public void setShow(boolean show)
    {
        this.show = show;
    }

    public String getPosition()
    {
        return StringUtils.lowerCase(position);
    }

    public void setPosition(String position)
    {
        this.position = Position.valueOf(StringUtils.upperCase(position)).toString();
    }

    public JSONObject toJson() throws JSONException
    {
        JSONObject thisObject = new JSONObject();
        thisObject.put("show", isShow());

        if (null != getPosition())
            thisObject.put("position", getPosition());

        return thisObject;
    }
}
