package net.customware.confluence.flotchart.model.options;


public class TimeSeriesTickSize implements AxisOptions.TickSize
{
    private final int size;

    private final TimeSeriesAxisOptions.TickSizeInterval interval;

    public TimeSeriesTickSize(int size, TimeSeriesAxisOptions.TickSizeInterval interval)
    {
        this.size = size;
        this.interval = interval;
    }

    public int getSize()
    {
        return size;
    }

    public TimeSeriesAxisOptions.TickSizeInterval getInterval()
    {
        return interval;
    }
}
