package net.customware.confluence.flotchart.model.options;

public class NumberTickSize implements AxisOptions.TickSize
{
    private final int size;

    public NumberTickSize(int size)
    {
        this.size = size;
    }

    public int getSize()
    {
        return size;
    }
}
