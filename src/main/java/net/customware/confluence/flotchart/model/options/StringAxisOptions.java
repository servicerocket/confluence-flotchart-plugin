package net.customware.confluence.flotchart.model.options;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.json.JSONException;

public class StringAxisOptions extends AxisOptions<NumberTickSize>
{
    private NumberTickSize minTickSize;

    private NumberTickSize tickSize;

    public StringAxisOptions()
    {
        setMinTickSize(new NumberTickSize(1));
    }

    public NumberTickSize getMinTickSize()
    {
        return minTickSize;
    }

    public void setMinTickSize(NumberTickSize minTickSize)
    {
        this.minTickSize = minTickSize;
    }

    public NumberTickSize getTickSize()
    {
        return tickSize;
    }

    public void setTickSize(NumberTickSize tickSize)
    {
        this.tickSize = tickSize;
    }

    public String toAxisValue(String stringValue)
    {
        return StringUtils.trim(stringValue);
    }

    public String getMode()
    {
        return null;
    }

    public boolean isScalable()
    {
        return false;
    }

    @Override
    public JSONObject toJson() throws JSONException
    {
        JSONObject thisObject = super.toJson();

        NumberTickSize minTickSize = getMinTickSize();
        if (null != minTickSize)
            thisObject.put("minTickSize", minTickSize.getSize());

        NumberTickSize tickSize = getTickSize();
        if (null != tickSize)
            thisObject.put("tickSize", tickSize.getSize());


        return thisObject;
    }
}
