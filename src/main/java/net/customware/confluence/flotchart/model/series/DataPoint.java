package net.customware.confluence.flotchart.model.series;

/**
 * Represents a single data point in a {@link Series}
 */
public class DataPoint implements Comparable<DataPoint>
{
    /** The x-axis value */
    private final Comparable domainValue;

    /** The y-axis value */
    private final Comparable rangeValue;

    /**
     * Creates a new instance of a data point
     * @param domainValue
     * The x-axis value. This may not be <tt>null</tt>.
     * @param rangeValue
     * The y-axis value. This <em>can</em> be <tt>null</tt>.
     */
    public DataPoint(Comparable domainValue, Comparable rangeValue)
    {
        this.domainValue = domainValue;
        this.rangeValue = rangeValue;
    }

    public Comparable getDomainValue()
    {
        return domainValue;
    }

    public Comparable getRangeValue()
    {
        return rangeValue;
    }

    public int compareTo(DataPoint o)
    {
        return domainValue.compareTo(o.getDomainValue());
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataPoint dataPoint = (DataPoint) o;

        if (domainValue != null ? !domainValue.equals(dataPoint.domainValue) : dataPoint.domainValue != null)
            return false;
        if (rangeValue != null ? !rangeValue.equals(dataPoint.rangeValue) : dataPoint.rangeValue != null) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = domainValue != null ? domainValue.hashCode() : 0;
        result = 31 * result + (rangeValue != null ? rangeValue.hashCode() : 0);
        return result;
    }
}
