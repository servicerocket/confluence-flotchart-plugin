package net.customware.confluence.flotchart;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.importexport.resource.DownloadResourceWriter;
import com.atlassian.confluence.importexport.resource.WritableDownloadResourceManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.core.util.DateUtils;
import com.atlassian.core.util.InvalidDurationException;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.opensymphony.webwork.ServletActionContext;
import net.customware.confluence.flotchart.model.Chart;
import net.customware.confluence.flotchart.model.options.AxisOptions;
import net.customware.confluence.flotchart.model.options.GridOptions;
import net.customware.confluence.flotchart.model.options.LegendOptions;
import net.customware.confluence.flotchart.model.options.NumberAxisOptions;
import net.customware.confluence.flotchart.model.options.PlotOptions;
import net.customware.confluence.flotchart.model.options.StringAxisOptions;
import net.customware.confluence.flotchart.model.options.TimeSeriesAxisOptions;
import net.customware.confluence.flotchart.model.series.BarCustomization;
import net.customware.confluence.flotchart.model.series.DataPoint;
import net.customware.confluence.flotchart.model.series.LineCustomization;
import net.customware.confluence.flotchart.model.series.PieCustomization;
import net.customware.confluence.flotchart.model.series.PointCustomization;
import net.customware.confluence.flotchart.model.series.Series;
import net.customware.confluence.flotchart.model.series.StepsCustomization;
import net.customware.confluence.flotchart.util.CellValue;
import net.customware.confluence.flotchart.util.DataUtil;
import net.customware.confluence.flotchart.util.FlotUtil;
import net.customware.confluence.flotchart.util.HtmlScraper;
import net.customware.confluence.flotchart.util.JFreeChartFactory;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.jfree.chart.JFreeChart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public class FlotChartMacro extends BaseMacro implements Macro
{
    private static final Logger LOG = LoggerFactory.getLogger(FlotChartMacro.class);

    private static final String CHART_TYPE_PIE = "pie";

    private static final String CHART_TYPE_BAR = "bar";

    private static final String CHART_TYPE_LINE = "line";

    private static final String CHART_TYPE_AREA = "area";

    private static final String CHART_TYPE_STEP = "step";

    private static final int DEFAULT_WIDTH = 384;

    private static final int DEFAULT_HEIGHT = 256;

    private static final double DEFAULT_PIE_COMBINE_THRESHOLD = 0.03;

    private static final Set<String> CHART_TYPES = new HashSet<String>(Arrays.asList(
            CHART_TYPE_PIE,
            CHART_TYPE_BAR,
            CHART_TYPE_LINE,
            CHART_TYPE_AREA,
            CHART_TYPE_STEP
    ));

    private static final Set<String> STATIC_RENDER_MODES = new HashSet<String>(
            Arrays.asList(
                    RenderContext.EMAIL,
                    RenderContext.FEED,
                    RenderContext.HTML_EXPORT,
                    RenderContext.PDF,
                    RenderContext.WORD
            )
    );

    private static final Pattern COLOR_PATTERN = Pattern.compile("(?i)#[0-9a-f]{6}");

    private final I18NBeanFactory i18NBeanFactory;

    private final LocaleManager localeManager;

    private final WritableDownloadResourceManager writableDownloadResourceManager;

    private final VelocityHelperService velocityHelperService;

    public FlotChartMacro(I18NBeanFactory i18NBeanFactory, LocaleManager localeManager, WritableDownloadResourceManager writableDownloadResourceManager, VelocityHelperService velocityHelperService)
    {
        this.i18NBeanFactory = i18NBeanFactory;
        this.localeManager = localeManager;
        this.writableDownloadResourceManager = writableDownloadResourceManager;
        this.velocityHelperService = velocityHelperService;
    }

    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context)
    {
        return TokenType.BLOCK;
    }

    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }

    public boolean hasBody()
    {
        return true;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.ALL;
    }

    public BodyType getBodyType()
    {
        return BodyType.RICH_TEXT;
    }

    private I18NBean getI18NBean()
    {
        return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.getUser()));
    }

    private String getText(String key)
    {
        return getI18NBean().getText(key);
    }

    private String getText(String key, List subs)
    {
        return getI18NBean().getText(key, subs);
    }

    private Locale getUserLocale(Map<String, String> macroParams)
    {
        String localeString = StringUtils.defaultString(macroParams.get("locale"));
        Locale defaultLocale = localeManager.getLocale(AuthenticatedUserThreadLocal.getUser());

        if (StringUtils.isNotBlank(localeString))
        {
            String[] localeTokens = StringUtils.split(localeString, "_");
            String language = localeTokens.length > 0 && StringUtils.isNotBlank(localeTokens[0]) && ArrayUtils.contains(Locale.getISOLanguages(), localeTokens[0])
                    ? localeTokens[0]
                    : defaultLocale.getLanguage();

            String country = localeTokens.length == 2 && StringUtils.isNotBlank(localeTokens[1]) && ArrayUtils.contains(Locale.getISOCountries(), StringUtils.upperCase(localeTokens[1]))
                    ? localeTokens[1]
                    : defaultLocale.getCountry();

            return new Locale(language, country);
        }

        return defaultLocale;
    }

    @SuppressWarnings("unchecked")
    public String execute(Map map, String s, RenderContext renderContext) throws MacroException
    {
        try
        {
            return execute(map, s, new DefaultConversionContext(renderContext));
        }
        catch (MacroExecutionException macroError)
        {
            throw new MacroException(macroError);
        }
    }

    public String execute(Map<String, String> _macroParams, String body, ConversionContext conversionContext) throws MacroExecutionException
    {
        boolean zoomAndPan = true;

        String chartType = getChartType(_macroParams);
        if (StringUtils.isBlank(chartType) || !CHART_TYPES.contains(chartType))
            return RenderUtils.blockError(
                    getText("flotchart.error.invalidcharttype"), ""
            );

        List<String> seriesColors = getSeriesColors(_macroParams);

        if (!isColorsArrayValid(seriesColors))
            return RenderUtils.blockError(getText("flotchart.error.invalidcolor"), "");

        boolean isTimeSeries = isTimeSeries(_macroParams);
        String timeSeriesValueFormat = isTimeSeries ? getTimeSeriesValueFormat(_macroParams) : null;
        String flotTimeFormat = null;
        if (StringUtils.isNotBlank(timeSeriesValueFormat))
        {
            try
            {
                // Check if the date format is valid
                new SimpleDateFormat(timeSeriesValueFormat);
            }
            catch (IllegalArgumentException e)
            {
                return RenderUtils.blockError(getText("flotchart.error.invaliddateformat", Arrays.asList(GeneralUtil.htmlEncode(timeSeriesValueFormat))), "");
            }

            flotTimeFormat = getFlotTimeFormat(_macroParams);
        }

        String numberFormat = getNumberFormat(_macroParams);
        if (StringUtils.isNotBlank(numberFormat))
        {
            try
            {
                // Check if numberFormat is valid.
                new DecimalFormat(numberFormat);
            }
            catch (IllegalArgumentException iae)
            {
                return RenderUtils.blockError(getText("flotchart.error.invalidnumberformat", Arrays.asList(numberFormat)), "");
            }
        }

        String xpathSelector = getTableXpathSelector(_macroParams);
        int[] tableNumberSelector = getTableNumberSelectors(_macroParams);

        if (null == xpathSelector && null == tableNumberSelector)
            xpathSelector = "//table"; // Select all tables by default

        try
        {

            List<CellValue[][]> grids = null != xpathSelector
                    ? new HtmlScraper().parseHtmlToGrids(body, xpathSelector)
                    : new HtmlScraper().parseHtmlToGrids(body, tableNumberSelector);

            if (null == grids || grids.isEmpty())
                return RenderUtils.blockError(getText("flotchart.error.cantfindtables"), "");

            Locale locale = getUserLocale(_macroParams);
            AxisOptions domainAxis = isTimeSeries
                    ? new TimeSeriesAxisOptions(locale, null, getTimeSeriesValueFormat(_macroParams), flotTimeFormat)
                    : new StringAxisOptions();
            NumberAxisOptions rangeAxis = new NumberAxisOptions(locale);
            rangeAxis.setNumberFormat(numberFormat);

            DataUtil dataUtil = new DataUtil(
                    domainAxis
                    , rangeAxis
                    , isDataVerticallyOriented(_macroParams) ? DataUtil.DataModel.VERTICAL : DataUtil.DataModel.HORIZONTAL,
                    StringUtils.equals(CHART_TYPE_PIE, chartType)
            );

            Chart theChart = dataUtil.toChart(grids);

            if (StringUtils.equals(CHART_TYPE_PIE, chartType) && isPieChartEmpty(theChart))
                return RenderUtils.blockError(getText("flotchart.error.emptypie"), "");

            seriesColors = FlotUtil.generateColorsForSeries(theChart.getSeries().size(), seriesColors);

            int colorIndex = 0;
            for (Series aSeries : theChart.getSeries())
            {
                if (colorIndex < seriesColors.size())
                    aSeries.setColor(seriesColors.get(colorIndex++));
                else
                    break;
            }

            PlotOptions plotOptions = theChart.getOptions();

            plotOptions.setLegendOptions(getLegendOptions(_macroParams));
            plotOptions.setGridOptions(getGridOptions(_macroParams));

            if (chartType.equals(CHART_TYPE_LINE))
                plotOptions.asLine(getLineCustomization( new LineCustomization(), _macroParams), isShowShapes(_macroParams) ? new PointCustomization() : null);
            else if (chartType.equals(CHART_TYPE_AREA))
                plotOptions.asLine(getAreaCustomization(new LineCustomization(), _macroParams), isShowShapes(_macroParams) ? new PointCustomization() : null);
            else if (chartType.equals(CHART_TYPE_STEP))
                plotOptions.asLine(getStepsCustomization(new StepsCustomization(), _macroParams), isShowShapes(_macroParams) ? new PointCustomization() : null);
            else if (chartType.equals(CHART_TYPE_BAR))
                plotOptions.asBar(getBarCustomization(new BarCustomization(), _macroParams));
            if (chartType.equals(CHART_TYPE_PIE))
            {
                // TODO: Enable zoom and pie once pie is better implemented.
                zoomAndPan = false;
                plotOptions.asPie(getPieCustomization(new PieCustomization(), _macroParams));
            }

            Map<String, Object> macroVelocityContext = getMacroVelocityContext();

            if (isStaticRenderMode(conversionContext))
            {
                String staticChartImagePath = getChartImageLink(theChart, _macroParams, conversionContext);
                if (null != staticChartImagePath)
                {
                    return new StringBuilder("<div><img src='").append(staticChartImagePath).append("'></div>").toString();
                }
                else
                {
                    return RenderUtils.blockError(
                            getText("flotchart.error.unabletogeneratestaticchart"), ""
                    );
                }
            }
            else
            {
                macroVelocityContext.put("chartJson", theChart.toJson().toString());
                macroVelocityContext.put("leftMargin", getLeftMargin(_macroParams));
                macroVelocityContext.put("width", getWidth(_macroParams) + "px");
                macroVelocityContext.put("height", getHeight(_macroParams) + "px");
                macroVelocityContext.put("showSeriesToggle", isShowSeriesToggle(_macroParams));
                macroVelocityContext.put("isBrowserInternetExplorer", isBrowserInternetExploiter());
                macroVelocityContext.put("zoom", zoomAndPan);
            }

            return renderChart(macroVelocityContext);
        }
        catch (UnsupportedEncodingException uee)
        {
            return RenderUtils.blockError(getText("flotchart.error.invalidencodingwhenreadinghtml"), "");
        }
        catch (ParseException pe)
        {
            return RenderUtils.blockError(getText("flotchart.error.invaliddateformat", Arrays.asList(GeneralUtil.htmlEncode(timeSeriesValueFormat))), "");
        }
        catch (Exception e)
        {
            LOG.error("Unable to render Flot Chart.", e);
            throw new MacroExecutionException(e);
        }
    }

    private boolean isStaticRenderMode(ConversionContext renderContext)
    {
        return STATIC_RENDER_MODES.contains(renderContext.getOutputType());
    }

    private String getTableXpathSelector(Map<String, String> macroParams)
    {
        if (BooleanUtils.toBoolean(macroParams.get("selectTablesWithXPath")))
            return macroParams.get("tables");

        return null;
    }

    private int[] getTableNumberSelectors(Map<String, String> macroParams)
    {
        if (!BooleanUtils.toBoolean(macroParams.get("selectTablesWithXPath")))
        {
            String[] tableNumbersString = StringUtils.split(macroParams.get("tables"), ",");
            if (null != tableNumbersString)
            {
                int[] tableNumbers = new int[tableNumbersString.length];
                for (int i = 0; i < tableNumbers.length; ++i)
                {
                    try
                    {
                        tableNumbers[i] = Integer.parseInt(tableNumbersString[i]);
                    }
                    catch (NumberFormatException nfe)
                    {
                        return null;
                    }
                }

                return tableNumbers;
            }
        }

        return null;
    }

    private boolean isDataVerticallyOriented(Map<String, String> macroParams)
    {
        return StringUtils.equalsIgnoreCase(DataUtil.DataModel.VERTICAL.toString(), macroParams.get("dataOrientation"));
    }

    private String getTimeSeriesValueFormat(Map<String, String> macroParams)
    {
        return macroParams.get("timeSeriesFormat");
    }

    private String getFlotTimeFormat(Map<String, String> macroParams)
    {
        return StringUtils.defaultString(macroParams.get("timeSeriesDisplayFormat"), null);
    }

    private boolean isTimeSeries(Map<String, String> macroParams)
    {
        return StringUtils.isNotBlank(getTimeSeriesValueFormat(macroParams));
    }

    private String getNumberFormat(Map<String, String> macroParams)
    {
        return macroParams.get("numberFormat");
    }

    private String getChartType(Map<String, String> macroParams)
    {
        return macroParams.get("type");
    }

    private Float getOpacity(Map<String, String> macroParams)
    {
        try
        {
            return Float.valueOf(StringUtils.trim(StringUtils.defaultString(macroParams.get("opacity"))));
        }
        catch (NumberFormatException nfe)
        {
            return null;
        }
    }

    private boolean isShowShapes(Map<String, String> macroParams)
    {
        return BooleanUtils.toBoolean(
                StringUtils.defaultString(
                        macroParams.get("showShapes"),
                        "true"
                )
        );
    }

    private boolean isShowLegend(Map<String, String> macroParams)
    {
        return BooleanUtils.toBoolean(
                StringUtils.defaultString(
                        macroParams.get("showLegend"),
                        "true"
                )
        );
    }

    private String getLegendPosition(Map<String, String> macroParams)
    {
        try
        {
            return StringUtils.lowerCase(
                    LegendOptions.Position.valueOf(
                            StringUtils.upperCase(StringUtils.defaultString(macroParams.get("legendPosition")))
                    ).toString()
            );
        }
        catch (IllegalArgumentException iae)
        {
            /* Invalid legend position specified. */
            return LegendOptions.Position.NE.toString();
        }
    }

    private String getLeftMargin(Map<String, String> macroParams)
    {
        return macroParams.get("leftMargin");
    }

    private int getWidth(Map<String, String> macroParams)
    {
        int width = DEFAULT_WIDTH;
        try
        {
            width = Integer.parseInt(StringUtils.trim(StringUtils.defaultString(macroParams.get("width"))));
            if (width < 1)
                width = DEFAULT_WIDTH;
        }
        catch (NumberFormatException nfe)
        {
            LOG.debug("Unable to convert specified width to an integer: " + macroParams.get("width"), nfe);
        }

        return width;
    }

    private int getHeight(Map<String, String> macroParams)
    {
        int height = DEFAULT_HEIGHT;
        try
        {
            height = Integer.parseInt(StringUtils.trim(StringUtils.defaultString(macroParams.get("height"))));
            if (height < 1)
                height = DEFAULT_HEIGHT;
        }
        catch (NumberFormatException nfe)
        {
            LOG.debug("Unable to convert specified height to an integer: " + macroParams.get("height"), nfe);
        }

        return height;
    }

    private boolean isShowSeriesToggle(Map<String, String> macroParams)
    {
        return BooleanUtils.toBoolean(macroParams.get("showSeriesToggle"));
    }

    private List<String> getSeriesColors(Map<String, String> macroParams)
    {
        String[] colorsArray = StringUtils.split(macroParams.get("colors"), ",");

        return (null != colorsArray)
                ? Arrays.asList(colorsArray)
                : Collections.<String> emptyList();
    }

    private boolean isColorsArrayValid(List<String> colors)
    {
        for (String color : colors)
            if (!COLOR_PATTERN.matcher(color).matches())
                return false;

        return true;
    }

    private double getBarWidth(Map<String, String> macroParams)
    {
        boolean isTimeSeries = isTimeSeries(macroParams);
        String barWidthString = StringUtils.defaultString(macroParams.get("barWidth"));
        double barWidth = isTimeSeries ?  DateUtils.DAY_MILLIS : 1;

        if (isTimeSeries(macroParams))
        {
            barWidth = DateUtils.DAY_MILLIS;
            try
            {
                barWidth = DateUtils.getDuration(barWidthString) * 1000;
            }
            catch (InvalidDurationException ide)
            {
                LOG.debug("Unable to convert barWidth to duration with " + barWidthString, ide);
            }
        }
        else
        {
            try
            {
                barWidth = Double.parseDouble(barWidthString);
            }
            catch (NumberFormatException nfe)
            {
                LOG.debug("Unable to convert barWidth to a number with " + barWidthString, nfe);
            }
        }

        return barWidth;
    }

    private boolean isHorizontalBarChart(Map<String, String> macroParams)
    {
//        return BooleanUtils.toBoolean(macroParams.get("horizontalBars"));
        return false;
    }

    private double getPieCombineThreshold(Map<String, String> macroParams)
    {
        double pieCombineThreshold = DEFAULT_PIE_COMBINE_THRESHOLD;
        try
        {
            pieCombineThreshold = Double.parseDouble(StringUtils.defaultString(macroParams.get("combineThreshold")));
            if (pieCombineThreshold < 0 || pieCombineThreshold > 1)
                pieCombineThreshold = DEFAULT_PIE_COMBINE_THRESHOLD;
        }
        catch (NumberFormatException nfe)
        {
            LOG.debug("Unable to convert pieCombineThreshold to a number with " + macroParams.get("combineThreshold"), nfe);
        }

        return pieCombineThreshold;
    }

    private String getPieOtherLabel(Map<String, String> macroParam)
    {
        return StringUtils.defaultString(macroParam.get("otherLabel"), null);
    }

    private Float getPieRadius(Map<String, String> macroParams)
    {
        Float pieRadius = null;
        try
        {
            pieRadius = new Float(StringUtils.defaultString(macroParams.get("pieRadius")));
            if (pieRadius < 0)
                pieRadius = null;
        }
        catch (NumberFormatException nfe)
        {
            LOG.debug("Unable to convert pieCombineThreshold to a number with " + macroParams.get("pieRadius"), nfe);
        }

        return pieRadius;
    }

    private boolean isBrowserInternetExploiter()
    {
        HttpServletRequest req = ServletActionContext.getRequest();
        return null != req && StringUtils.defaultString(req.getHeader("User-Agent")).matches("(?i)mozilla/\\d\\.\\d+\\s+\\(\\s*compatible;\\s+msie\\s+\\d\\.\\d+[a-z0-9]*;.*\\)");
    }

    private <T extends LineCustomization> T getLineCustomization(T lineCustomization, Map<String, String> macroParams)
    {
        lineCustomization.setFill(getOpacity(macroParams));
        return lineCustomization;
    }

    private PieCustomization getPieCustomization(PieCustomization pieCustomization, Map<String, String> macroParams)
    {
        getLineCustomization(pieCustomization, macroParams);

        pieCustomization.setCombineThreshold(getPieCombineThreshold(macroParams));
        pieCustomization.setCombineLabel(getPieOtherLabel(macroParams));
        pieCustomization.setRadius(getPieRadius(macroParams));

        if (!isShowLegend(macroParams))
            pieCustomization.setShowLabel(false);

        return pieCustomization;
    }

    private BarCustomization getBarCustomization(BarCustomization barCustomization, Map<String, String> macroParams)
    {
        getLineCustomization(barCustomization, macroParams);
        barCustomization.setBarWidth(getBarWidth(macroParams));
        barCustomization.setHorizontal(isHorizontalBarChart(macroParams));
        return barCustomization;
    }

    private LineCustomization getAreaCustomization(LineCustomization lineCustomization, Map<String, String> macroParams)
    {
        getLineCustomization(lineCustomization, macroParams);

        Float opacity = getOpacity(macroParams);
        lineCustomization.setFill(null == opacity ? 0.5f : opacity);
        return lineCustomization;
    }

    private StepsCustomization getStepsCustomization(StepsCustomization stepsCustomization, Map<String, String> macroParams)
    {
        getLineCustomization(stepsCustomization, macroParams);
        stepsCustomization.setShow(true);
        return stepsCustomization;
    }

    private LegendOptions getLegendOptions(Map<String, String> macroParams)
    {
        LegendOptions legendOptions = new LegendOptions();

        legendOptions.setShow(isShowLegend(macroParams));
        legendOptions.setPosition(getLegendPosition(macroParams));
        return legendOptions;
    }

    String renderChart(Map<String, Object> macroVelocityContext)
    {
        return velocityHelperService.getRenderedTemplate("templates/net/customware/confluence/flotchart/flotchart.vm", macroVelocityContext);
    }

    Map<String, Object> getMacroVelocityContext()
    {
        return velocityHelperService.createDefaultVelocityContext();
    }


/// JFreeChart code --- For static render modes.

    private String getChartImageAsUrl(BufferedImage image) throws IOException
    {
        OutputStream out = null;

        try
        {
            DownloadResourceWriter downloadResourceWriter = writableDownloadResourceManager.getResourceWriter(AuthenticatedUserThreadLocal.getUsername(), "flotchart", ".png");
            out = downloadResourceWriter.getStreamForWriting();

            ImageIO.write(image, "png", out);

            return downloadResourceWriter.getResourcePath();
        }
        finally
        {
            IOUtils.closeQuietly(out);
        }
    }

    private String getChartImageAsDataUrl(BufferedImage image) throws IOException
    {
        ByteArrayOutputStream out = null;

        try
        {
            out = new ByteArrayOutputStream();
            ImageIO.write(image, "png", out);

            return new StringBuilder("data:image/png;charset=US-ASCII;base64,").append(new String(Base64.encodeBase64(out.toByteArray(), true), "US-ASCII")).toString();
        }
        finally
        {
            IOUtils.closeQuietly(out);
        }
    }

    private String getChartImageLink(Chart theChart, Map<String, String> macroParams, ConversionContext conversionContext) throws IOException
    {
        String chartType = getChartType(macroParams);
        JFreeChart theJFreeChart = null;
        JFreeChartFactory jFreeChartFactory = new JFreeChartFactory(i18NBeanFactory);

        if (StringUtils.equals(CHART_TYPE_PIE, chartType))
            theJFreeChart = jFreeChartFactory.createPieChart(theChart);
        else if (StringUtils.equals(CHART_TYPE_LINE, chartType))
            theJFreeChart = jFreeChartFactory.createLineChart(theChart);
        else if (StringUtils.equals(CHART_TYPE_BAR, chartType))
            theJFreeChart = jFreeChartFactory.createBarChart(theChart);
        else if (StringUtils.equals(CHART_TYPE_AREA, chartType))
            theJFreeChart = jFreeChartFactory.createAreaChart(theChart);
        else if (StringUtils.equals(CHART_TYPE_STEP, chartType))
            theJFreeChart = jFreeChartFactory.createStepChart(theChart);

        if (null != theJFreeChart)
        {
            BufferedImage image = theJFreeChart.createBufferedImage(getWidth(macroParams), getHeight(macroParams));
            return RenderContext.HTML_EXPORT.equals(conversionContext.getOutputType())
                    || RenderContext.EMAIL.equals(conversionContext.getOutputType())
                    ? getChartImageAsDataUrl(image)
                    : getChartImageAsUrl(image);
        }

        return null;
    }

    private boolean isShowDataPointvalue(Map<String, String> macroParams)
    {
        return BooleanUtils.toBoolean(macroParams.get("showDataPointValue"));
    }

    private GridOptions getGridOptions(Map<String, String> macroParams)
    {
        GridOptions gridOptions = new GridOptions();

        boolean showShapes = isShowShapes(macroParams);
        boolean showDataPointValue = isShowDataPointvalue(macroParams);
        boolean isBarChart = StringUtils.equals(CHART_TYPE_BAR, getChartType(macroParams));

        gridOptions.setHoverable(!isBarChart && showShapes && showDataPointValue);
        gridOptions.setClickable(!isBarChart && showShapes && showDataPointValue);

        return gridOptions;
    }

    private boolean isPieChartEmpty(Chart theChart)
    {
        Collection<Series> dataSeries = theChart.getSeries();

        if (null != dataSeries)
        {
            for (Series series : dataSeries)
            {
                Collection<DataPoint> dataPoints  = series.getData();
                if (null != dataPoints)
                {
                    for (DataPoint dataPoint : dataPoints)
                    {
                        if (null != dataPoint && null != dataPoint.getRangeValue())
                            return false;
                    }
                }
            }
        }

        return true;
    }
}
